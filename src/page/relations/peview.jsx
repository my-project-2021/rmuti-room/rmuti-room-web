import React, { useState, useEffect } from "react";
import { Button, Card, Row, Col } from "antd";
import { useNavigate, useParams } from "react-router-dom";
import { getData } from "../../core/action/collection";
import { BaseURL } from "../../baseURL";
import styled from "styled-components";
export default function Peview() {
  const navigate = useNavigate();
  const [data, setData] = useState();
  const { id } = useParams();
  useEffect(() => {
    getDataNews();
  }, []);

  const getDataNews = async () => {
    const res = await getData("/news");
    const dataItem = res?.filter((value) => value?.news_id === id);
    setData(dataItem);
  };
  return (
    <div style={{ padding: "20px 50px 20px 50px" }}>
      <Button onClick={() => navigate(-1)}>ย้อนกลับ</Button>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          objectFit: "cover",
          width: "100%",
          height: "500px",
        }}
      >
        <img src={`${BaseURL.ImageURL}${data && data[0]?.cover_image}`} alt="" />
      </div>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          paddingTop: "20px",
        }}
      >
        <div>
          <h2>หัวข้อข่าว</h2>
          <p>{data && data[0]?.topic}</p>
          <h2>เนื้อหา</h2>
          <p>{data && data[0]?.content}</p>
          <BoxImg>
            <div className="flex-container wrap">
              <div className="object">
                {data &&
                  data[0]?.images.split(",").map((value) => {
                    return <img className="flex-item" src={`${BaseURL.ImageURL}${value}`} alt="images" />;
                  })}
              </div>
            </div>
          </BoxImg>
        </div>
      </div>
    </div>
  );
}

const BoxImg = styled.div`
  width: 100%;
  .flex-container {
    display: flex;
    width: 1000px;
  }
  .wrap {
    flex-wrap: wrap;
  }
  .flex-item {
    height: 300px;
    width: 490px;
    margin: 10px 0 0 10px;
    object-fit: cover;
  }
`;
