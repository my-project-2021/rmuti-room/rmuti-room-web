import React, { useEffect, useState } from "react";
import { Layout, Button, Row, Col, Carousel, Card, Tooltip } from "antd";
import { getData, postData } from "../../core/action/collection";
import { BaseURL } from "../../baseURL";
import styled from "styled-components";
import { useNavigate } from "react-router-dom";
import Logo from "../../assets/images//png/RMUTI-logo-color2-1.png";
import { CreateLog } from "../../core/util/createlog";
const { Header, Content, Footer } = Layout;

const RelationPage = (props) => {
  const location = window.location.pathname.split("/");
  const [data, setData] = useState();
  const navigate = useNavigate();
  const [dataUser, setDataUser] = useState();

  useEffect(() => {
    getDataNews();
    if (location[location.length - 2] === "about" && location[location.length - 1]) {
      localStorage.setItem("id", location[location.length - 1]);
      getDataUser().then(() => CreateLog("login"));
      setTimeout(() => {
        window.location.reload();
      }, 1000);
    } else if (location[location.length - 1] === "waiting" && !location[location.length - 1]) {
      window.location.replace("/pws/web/waiting");
    } else if (location[location.length - 2] === "waiting" && location[location.length - 1]) {
      window.location.replace("/pws/web/role/" + location[location.length - 1]);
    } else if (location[location.length - 2] === "register") {
      localStorage.setItem("id", location[location.length - 1]);
      getDataUser();
      window.location.replace("/pws/web/register/" + location[location.length - 1]);
    } else {
      localStorage.clear();
    }
  }, []);

  const getDataUser = async () => {
    const res = await getData(`/user/${location[location.length - 1]}`);
    const resBooking = await getData("/booking");
    const dataFilter = resBooking?.filter((item) => item.user_id === localStorage.getItem("id"));
    setDataUser(res);
    localStorage.setItem("role", res?.type_id);
    localStorage.setItem("room_id", dataFilter[0]?.room_id);
    localStorage.setItem("status_id", res?.status_id);
  };

  const getDataNews = async () => {
    const res = await getData("/news");
    setData(res);
  };

  const loginSSO = () => {
    window.location.replace(BaseURL.loginSSO);
  };

  return (
    <Layout className="layout" style={{ minHeight: "100vh" }}>
      <Header>
        <Row justify="space-between">
          <Col span={12}>
            <div style={{ display: "flex" }}>
              <div>
                <img
                  src={"https://rmuti.ac.th/one/wp-content/uploads/2021/12/RMUTI_KORAT.png"}
                  width="40"
                  height="60"
                  alt="Logo"
                />
              </div>
              <div>
                <span className="logo">RMUTI Room</span>
              </div>
            </div>
          </Col>
          <Col span={12} style={{ textAlign: "end" }}>
            <Button type="primary" style={{ borderRadius: "10px", width: 120, height: 40 }} onClick={() => loginSSO()}>
              <span> Login</span>
            </Button>
          </Col>
        </Row>
      </Header>
      <Content>
        <Box>
          <Carousel autoplay>
            <div className="content-style">
              <img
                className="responsive"
                src="https://lh3.googleusercontent.com/p/AF1QipMzyjIeNPoZ5GgjUCsYdDDXEB4FrwB8cIQ9Ga1i=s1360-w1360-h1020"
              />
            </div>
            <div className="content-style">
              <img
                className="responsive"
                src="https://lh3.googleusercontent.com/p/AF1QipMxO6kgw6qAnoSxa6HiZZzV1LQ8A81DkoqgPDEu=s1360-w1360-h1020"
              />
            </div>
            <div className="content-style">
              <img
                className="responsive"
                src="https://lh3.googleusercontent.com/p/AF1QipMifytmZ3VyIEPrlUlih0v3PUmI_N3UaSYEZKRs=s1360-w1360-h1020"
              />
            </div>
            <div className="content-style">
              <img
                className="responsive"
                src="https://lh3.googleusercontent.com/p/AF1QipMjE0M8WRcHbKVOyw3OWdLiP7dUyla73F20vBla=s1360-w1360-h1020"
              />
            </div>
          </Carousel>
        </Box>

        <div>
          <div class="row">
            {data?.map((item) => {
              return (
                <div class="column" style={{ margin: "60px" }}>
                  <Card
                    style={{
                      borderRadius: "10px",
                      width: "400px",
                      height: "300px",
                    }}
                    title={item.topic}
                    bordered={false}
                    actions={[
                      <TextNavigate onClick={() => navigate(`peview-news/${item.news_id}`)}>อ่านต่อคลิก</TextNavigate>,
                    ]}
                  >
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        objectFit: "cover",
                        width: "100%",
                        height: "100px",
                      }}
                    >
                      <img src={`${BaseURL.ImageURL}${item.cover_image}`} />
                    </div>
                    <TextDetail className="text">
                      <Tooltip placement="topLeft" title={item?.content}>
                        <p>{item.content}</p>
                      </Tooltip>
                    </TextDetail>
                  </Card>
                </div>
              );
            })}
          </div>
        </div>
      </Content>
    </Layout>
  );
};

export default RelationPage;

const TextDetail = styled.div`
  padding-top: 20px;
  display: -webkit-box;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
  overflow: hidden;
  height: 75px;
`;

const TextNavigate = styled.p`
  cursor: pointer;
`;

const Box = styled.div`
  .responsive {
    width: 100%;
    height: 100%;
  }
`;
