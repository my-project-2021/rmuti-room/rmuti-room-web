import React, { useEffect, useState } from "react";
import { getData } from "../../core/action/collection";
import { useNavigate, useParams } from "react-router-dom";
import styled from "styled-components";
import { manageDate } from "../../component/table//manageDate";
import { Button } from "antd";
export default function Index() {
  const { id } = useParams();
  const navigate = useNavigate();
  const [dataBooking, setDataBooking] = useState();
  useEffect(() => {
    getDataBooking();
  }, []);

  const getDataBooking = async () => {
    const res = await getData("/booking");
    const result = res?.filter((item) => item?.room_id === id);
    console.log("result", result);
    setDataBooking(result);
  };

  return (
    <div>
      <div style={{ paddingBottom: "20px", paddingTop: "20px" }}>
        <Button style={{ borderRadius: "10px", width: 120, height: 40 }} onClick={() => navigate(-1)}>
          <span>ย้อนกลับ</span>
        </Button>
      </div>
      <div style={{ paddingLeft: "100px" }}>
        <Text>ชื่ออาคาร </Text>
        <Text textIndent="50px">{dataBooking && dataBooking[dataBooking?.length - 1]?.building_name} </Text>
        <Text>ชื่อห้อง </Text>
        <Text textIndent="50px">{dataBooking && dataBooking[dataBooking?.length - 1]?.room_name} </Text>
        <Text>ชื่อ-นามสกุล</Text>
        <Text textIndent="50px">
          <span style={{ paddingRight: "10px" }}>{dataBooking && dataBooking[dataBooking?.length - 1]?.fname}</span>
          <span>{dataBooking && dataBooking[dataBooking?.length - 1]?.lname}</span>
        </Text>
        <Text>เบอร์โทร </Text>
        <Text textIndent="50px">{dataBooking && dataBooking[dataBooking?.length - 1]?.tel}</Text>
        <Text>วันเข้าอยู่ </Text>
        <Text textIndent="50px">{dataBooking && manageDate(dataBooking[dataBooking?.length - 1]?.check_in)} </Text>
        <Text>วันออก </Text>
        <Text textIndent="50px">{dataBooking && manageDate(dataBooking[dataBooking?.length - 1]?.check_out)} </Text>
        <Text>สถานะ </Text>
        <Text textIndent="50px">{dataBooking && dataBooking[dataBooking?.length - 1]?.type_name} </Text>
      </div>
    </div>
  );
}

const Text = styled.p`
  font-size: 18px;
  text-indent: ${(props) => (props.textIndent ? props.textIndent : "0")};
`;
