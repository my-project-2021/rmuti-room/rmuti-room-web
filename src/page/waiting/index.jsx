import React, { useEffect, useState } from "react";
import { Result, Button } from "antd";
import { BaseURL } from "../../baseURL";
import { getData } from "../../core/action/collection";

export default function Index() {
  const location = window.location.pathname.split("/");
  const [Status, setStatus] = useState();

  const onLogout = async () => {
    await localStorage.clear();
    await window.location.replace(BaseURL.logoutSSO);
  };

  useEffect(() => {
    getUser();
  }, []);

  const getUser = async () => {
    const res = await getData(`user/${location[location.length - 1]}`);
    setStatus(res?.status_id);
  };

  return (
    <div style={{ padding: "100px" }}>
      {Status && Status === "3" ? (
        <Result
          status="error"
          title="รหัสของคุณถูกแอดมินปฏิเสธให้ใช้งาน"
          extra={[
            <Button type="primary" onClick={() => onLogout()}>
              ออกจากระบบ
            </Button>,
          ]}
        />
      ) : (
        <Result
          status="success"
          title="รอแอดมินอนุมัติให้ใช้งาน"
          extra={[
            <Button type="primary" onClick={() => onLogout()}>
              ออกจากระบบ
            </Button>,
          ]}
        />
      )}
    </div>
  );
}
