import React, { useState, useEffect } from "react";
import TableUser from "../../component/table/";
import { getData, postData } from "../../core/action/collection";
import ModalUser from "../../component/modal/";
import { Button, Input, Form, Select, DatePicker, message, Dropdown, Menu, Popconfirm, Radio, Modal } from "antd";
import { manageDate } from "../../component/table/manageDate";
import { BarsOutlined, EditOutlined, DeleteOutlined, FileTextOutlined } from "@ant-design/icons";
import moment from "moment";
import { CreateLog } from "../../core/util/createlog";
import styled from "styled-components";
const { Option } = Select;
export default function Index() {
  const [dataUser, setDataUser] = useState();
  const [showModal, setShowModal] = useState(false);
  const [bookingId, setBookingId] = useState();
  const [dataBuilding, setDataBuilding] = useState();
  const [roomId, setRoomId] = useState();
  const [builddingId, setBuilddingId] = useState();
  const [user, setUser] = useState();
  const [statusUser, setStatusUser] = useState();
  const [dataRoom, setDataRoom] = useState();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [form] = Form.useForm();

  const menu = (data) => (
    <Menu>
      <Menu.Item>
        <p onClick={() => updateUser(data)}>
          <span style={{ paddingRight: "10px" }}>
            <EditOutlined />
          </span>
          แก้ไข
        </p>
      </Menu.Item>
      <Menu.Item>
        <p>
          <BoxPopupConfirm
            className="popup-booking"
            title="หากลบข้อมูลออกไปแล้วจะไม่สามารถ เข้าใช้งานได้จนกว่าจะทำงานจองและ อนุมัติใหม่อีกครั้ง"
            okText="ตกลง"
            onConfirm={() => deleteUser(data)}
            cancelText="ยกเลิก"
          >
            <span style={{ paddingRight: "10px" }}>
              <DeleteOutlined />
            </span>
            ลบ
          </BoxPopupConfirm>
        </p>
      </Menu.Item>
      <Menu.Item>
        <p
          onClick={() => {
            setIsModalVisible(true);
            setUser(data);
            setStatusUser(data?.status_id);
          }}
        >
          <span style={{ paddingRight: "10px" }}>
            <FileTextOutlined />
          </span>
          จัดการ
        </p>
      </Menu.Item>
    </Menu>
  );

  const columns = [
    {
      title: <p>ลำดับ</p>,
      width: 50,
      render: (_, data, index) => <div>{index + 1}</div>,
    },
    {
      title: <p>ข้อมูลผู้ใช้</p>,
      width: 150,
      render: (_, data) => (
        <div>
          <p> {data.fname}</p>
          <p> {data.lname}</p>
          <p>เบอร์:{data.tel}</p>
        </div>
      ),
    },
    {
      title: <p>ข้อมูลห้อง</p>,
      width: 150,
      key: "",
      render: (_, data) => (
        <div>
          <p> {data.building_name ? data.building_name : "-"}</p>
          <p> {data.room_name ? data.room_name : "-"}</p>
        </div>
      ),
    },
    {
      title: <p>วันเข้าอยู่</p>,
      width: 150,
      render: (_, data, index) => <p>{data?.check_in ? manageDate(data.check_in) : "-"}</p>,
    },
    {
      title: <p>วันออก</p>,
      width: 150,
      render: (_, data) => <p>{data?.check_out ? manageDate(data.check_out) : "-"}</p>,
    },
    {
      title: <p>สถานะ</p>,
      dataIndex: "response_detail",
      width: 110,
      render: (_, data) => (
        <div>
          <span>{data?.status_name}</span>
        </div>
      ),
    },
    {
      title: <p>สิทธิ์การเข้าใช้</p>,
      dataIndex: "type_name",
      width: 110,
      render: (_, data, index) => <p>{data?.type_name}</p>,
    },
    {
      title: <p>จัดการ</p>,
      width: 100,
      render: (_, data) => <Dropdown.Button icon={<BarsOutlined />} overlay={menu(data)} />,
    },
  ];

  useEffect(() => {
    getDataUsers();
    getRoom();
    getBuilding();
  }, []);

  const getRoom = async (e) => {
    if (e) {
      const res = await getData(`room`);
      const data = res.filter((value, index, array) => value?.building_id === e);
      setDataRoom(data);
      form.setFieldsValue({ room_id: data[0]?.room_id });
    } else {
      const res = await getData(`room`);
      setDataRoom(res);
    }
  };

  const getBuilding = async () => {
    const res = await getData("building");
    setDataBuilding(res);
  };

  const updateUser = async (data) => {
    const res = await getData(`room`);
    const dataRoom = res.filter((value, index, array) => value?.building_id === data?.building_id);
    setDataRoom(dataRoom);
    setBookingId(data?.booking_id);
    setStatusUser(data?.type_id);
    setShowModal(true);
    setRoomId(data?.room_id);
    form.setFieldsValue({
      fname: data.fname,
      lname: data.lname,
      role: data?.type_id,
      building_name: data?.building_id,
      room_id: data?.room_id,
      check_in: moment(data.check_in, "YYYY-MM-DD"),
      check_out: moment(data.check_out, "YYYY-MM-DD"),
    });
  };

  const deleteUser = async (data) => {
    const test = dataRoom.filter((item) => item.room_id === data.room_id);
    if (test?.length) {
      const bodyRoom = {
        building_id: data.building_id,
        room_name: data.room_name,
        room_status: 0,
        water_unit: test[0].water_unit,
        power_unit: test[0].power_unit,
      };
      const resp = await postData(`/room/${data?.room_id}`, { ...bodyRoom });
    }
    const res = await postData(`user/delete/${data.user_id}`);
    message.success("ลบข้อมูลสำเร็จ");
    getDataUsers();
    CreateLog("ลบผู้ใช้งาน");
  };

  const getDataUsers = async () => {
    const res = await getData("user");
    const data = res?.map((item, index) => ({ ...item, key: index + 1 }));
    setDataUser(data);
  };

  const confirmUser = async () => {
    const body = {
      status_id: statusUser,
    };

    const bodyBooking = {
      booking_status: user.booking_status === "0" ? "1" : "0",
    };

    const res = await postData(`user/update_status/${user?.user_id}`, body);
    await postData(`booking/update_status/${user.booking_id}`, bodyBooking);
    if (res?.data?.code === 200) {
      const test = dataRoom.filter((item) => item.room_id === user.room_id);
      const bodyRoom = {
        building_id: user.building_id,
        room_name: user.room_name,
        room_status: statusUser === "3" ? 0 : 1,
        water_unit: test[0].water_unit,
        power_unit: test[0].power_unit,
      };
      const resp = await postData(`/room/${user?.room_id}`, { ...bodyRoom });
      message.success("เปลี่ยนคำร้องขอเรียบร้อย");
      CreateLog("เปลี่ยนคำร้องขอผู้ใช้งาน");
      getDataUsers();
      setUser();
      setIsModalVisible(false);
    } else {
      message.error("ห้องนี้ถูกอนุมัติไปแล้ว โปรดเลือกห้องอื่น");
    }
  };

  const onUpdateBooking = async (value) => {
    const test = dataRoom?.filter((item) => item.room_id === value.room_id);
    const testRoom = dataRoom?.filter((item) => item.room_id === roomId);
    const body = {
      check_in: moment(value.check_in._d).format("YYYY-MM-DD"),
      check_out: moment(value.check_out._d).format("YYYY-MM-DD"),
      room_id: value.room_id,
      type_id: value?.role,
    };
    const res = await postData(`booking/${bookingId}`, body);
    if (res.data.code === 200) {
      message.success("แก้ไขเรียบร้อย");
      CreateLog("แก้ไขผู้ใช้งาน");
      getDataUsers();
      setShowModal(false);
    } else {
      message.error("ห้องนี้มีผู้พักแล้ว");
      setShowModal(false);
    }
    if (value?.room_id !== roomId) {
      const bodyRoom = {
        building_id: value.building_name,
        room_name: test[0].room_name,
        room_status: 1,
        water_unit: test[0].water_unit,
        power_unit: test[0].power_unit,
      };
      const resp = await postData(`/room/${value?.room_id}`, { ...bodyRoom });
      const body = {
        building_id: value.building_name,
        room_name: testRoom[0].room_name,
        room_status: 0,
        water_unit: testRoom[0].water_unit,
        power_unit: testRoom[0].power_unit,
      };
      const res = await postData(`/room/${roomId}`, { ...body });
    }
  };

  const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
  };

  const FormUpdateBooking = () => {
    return (
      <>
        <Form name="updateBooking" form={form} {...layout} autoComplete="off" onFinish={onUpdateBooking}>
          <Form.Item label={<span>ชื่อ</span>} name="fname">
            <Input disabled />
          </Form.Item>
          <Form.Item label={<span>นามสกุล</span>} name="lname">
            <Input disabled />
          </Form.Item>
          {statusUser !== "1003" ? (
            <div>
              <Form.Item
                label={<span>ชื่ออาคาร</span>}
                name="building_name"
                rules={[{ required: true, message: "กรุณากรอก ชื่ออาคาร!" }]}
              >
                <Select allowClear onChange={(e) => getRoom(e)}>
                  {dataBuilding &&
                    dataBuilding?.map((item) => {
                      return (
                        <>
                          <Option value={item.building_id}>{item.building_name}</Option>
                        </>
                      );
                    })}
                </Select>
              </Form.Item>
              <Form.Item
                label={<span>ชื่อห้อง</span>}
                name="room_id"
                rules={[{ required: true, message: "กรุณากรอก ชื่อห้อง!" }]}
              >
                <Select allowClear value={roomId}>
                  {dataRoom &&
                    dataRoom?.map((item) => {
                      return (
                        <>
                          <Option value={item?.room_id}>{item?.room_name}</Option>
                        </>
                      );
                    })}
                </Select>
              </Form.Item>
              <Form.Item label={<span>วันที่เข้าอยู่</span>} name="check_in">
                <DatePicker />
              </Form.Item>
              <Form.Item label={<span>วันที่ออก</span>} name="check_out">
                <DatePicker />
              </Form.Item>
            </div>
          ) : (
            ""
          )}
          <Form.Item
            label={<span>สิทธิ์เข้าใช้งาน</span>}
            name="role"
            rules={[{ required: true, message: "กรุณากรอก สิทธิ์เข้าใช้งาน!" }]}
          >
            <Select allowClear onChange={(e) => setStatusUser(e)}>
              <Option value="1001">ผู้พักอาศัย</Option>
              <Option value="1002">กรรมการหอพัก</Option>
              <Option value="1003">ผู้ดูแลระบบ</Option>
            </Select>
          </Form.Item>
          <div style={{ display: "flex", justifyContent: "end" }}>
            <Form.Item>
              <Button style={{ borderRadius: "10px", width: 120, height: 40 }} type="primary" htmlType="submit">
                <span>บันทึก</span>
              </Button>
            </Form.Item>
          </div>
        </Form>
      </>
    );
  };

  return (
    <div>
      <h2>จัดการข้อมูลผู้พักอาศัย</h2>
      <TableUser columns={columns} dataSource={dataUser} />
      <ModalUser showModal={showModal} setShowModal={setShowModal} textTitle="จัดการผู้ใช้">
        <FormUpdateBooking />
      </ModalUser>
      <Modal visible={isModalVisible} onOk={() => confirmUser()} onCancel={() => setIsModalVisible(false)}>
        <Radio.Group onChange={(e) => setStatusUser(e.target.value)} value={statusUser}>
          <Radio value={"1"}>รอการอนุมัติ</Radio>
          <Radio value={"2"}>อนุมัติ</Radio>
          <Radio value={"3"}>ไม่อนุมัติ</Radio>
        </Radio.Group>
      </Modal>
    </div>
  );
}

const BoxPopupConfirm = styled(Popconfirm)`
  .popup-booking {
    .ant-popover-buttons button {
      margin-left: 8px;
      height: 35px !important;
    }
  }
`;
