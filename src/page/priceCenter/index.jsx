import React, { useState, useEffect } from "react";
import TablePriceCenter from "../../component/table/";
import { getData, postData } from "../../core/action/collection";
import ModalPrice from "../../component/modal/";
import { Button, Input, Form, Select, DatePicker, message } from "antd";
import moment from "moment";
import { manageDate } from "../../component/table/manageDate";
import { CreateLog } from "../../core/util/createlog";
const { RangePicker } = DatePicker;
const { Option } = Select;
export default function Index() {
  const [showModal, setShowModal] = useState(false);
  const [dataBuilding, setDataBuilding] = useState();
  const [dataRoom, setDataRoom] = useState();
  const [dataCom, setDataCom] = useState();
  const [date, setDate] = useState();
  const [form] = Form.useForm();
  const columns = [
    {
      title: <p>ลำดับ</p>,
      width: 50,
      render: (_, data, index) => <div>{index + 1}</div>,
    },
    {
      title: <p>ข้อมูลห้อง</p>,
      width: 150,
      render: (_, data, index) => (
        <div>
          <p> {data?.building_name}</p>
          <p> {data?.room_name}</p>
        </div>
      ),
    },
    {
      title: <p>ค่าส่วนกลาง</p>,
      width: 150,
      render: (_, data, index) => (
        <div>
          <p>{data?.commonfee_price}</p>
        </div>
      ),
    },
    {
      title: <p>จำนวนที่จ่าย</p>,
      width: 150,
      render: (_, data, index) => (
        <div>
          <p>{` ${moment(data?.bill_month_start).format("DD/MM/YYYY")} - ${moment(data?.bill_month_end).format(
            "DD/MM/YYYY"
          )}`}</p>
          <p>รวม {data?.commonfee_price_sum}</p>
        </div>
      ),
    },
    {
      title: <p>สถานะ</p>,
      width: 150,
      render: (_, data, index) => (
        <div>
          <p>{data?.status === "0" ? "ยังไม่ชำระเงิน" : "ชำระเงินแล้ว"}</p>
          <p style={{ fontSize: "12px" }}>{data.date_pay ? manageDate(data?.date_pay) : ""}</p>
          <p style={{ fontSize: "12px" }}>{data.name_create ? `สร้าง:${data.name_create}` : ""}</p>
          <p style={{ fontSize: "12px" }}>{data.name_pay ? `ชำระเงิน:${data.name_pay}` : ""}</p>
        </div>
      ),
    },
    {
      title: <p>จัดการ</p>,
      width: 150,
      render: (_, data, index) =>
        data?.status === "0" && localStorage.getItem("role") === "1002" ? (
          <div>
            <Button type="primary" onClick={() => checkBill(data?.bill_commonfee_id)}>
              ชำระเงิน
            </Button>
          </div>
        ) : (
          ""
        ),
    },
  ];
  useEffect(() => {
    getBuilding();
    getRoom();
    getDataCom();
  }, [showModal]);

  const getDataCom = async (value) => {
    if (localStorage.getItem("role") === "1001") {
      let paramSearch = value ? `?month=${value}` : "";
      const resUser = await getData(`/booking`);
      const dataUser = resUser?.filter((value, index, array) => value?.user_id === localStorage.getItem("id"));
      const resBulding = await getData(`/building`);
      const dataBuildingFilter = resBulding?.filter(
        (value, index, array) => value?.building_id === dataUser[0]?.building_id
      );
      const res = await getData(`billCommonfee` + paramSearch);
      const resCom = res?.filter((item) => item?.room_id === dataUser[0]?.room_id);

      setDataCom(resCom);
      showModal &&
        form.setFieldsValue({
          building_name: dataUser[0]?.building_id,
          room_id: dataUser[0]?.room_id,
          com: dataBuildingFilter[0]?.commonfee_price,
        });
    } else {
      let paramSearch = value ? `?month=${value}` : "";
      const resUser = await getData(`/booking`);
      const dataUser = resUser?.filter((value, index, array) => value?.user_id === localStorage.getItem("id"));
      const resBulding = await getData(`/building`);
      const dataBuildingFilter = resBulding?.filter(
        (value, index, array) => value?.building_id === dataUser[0]?.building_id
      );
      const res = await getData(`billCommonfee` + paramSearch);
      const resCom = res?.filter((item) => item?.building_id === dataBuildingFilter[0]?.building_id);
      setDataCom(resCom);
    }
  };

  const checkBill = async (id) => {
    const resUser = await getData("user");
    const userData = resUser.filter((item) => item?.user_id === localStorage.getItem("id"));
    const body = {
      name: `${userData[0]?.fname} ${userData[0]?.lname} ${userData[0]?.type_name} ${userData[0]?.building_name}`,
    };
    const res = await postData(`billCommonfee/payment/${id}`, { ...body });
    if (res?.data?.code === 200) {
      message.success("ชำระเงินเรียบร้อย", 2);
      CreateLog("ชำระเงินส่วนกลาง");
      getDataCom();
    }
  };

  const onChangePicker = (value) => {
    let text = value ? `${moment(value[0]).format("YYYY-MM")}/${moment(value[1]).format("YYYY-MM")}` : "";
    getDataCom(text);
  };
  const changeRoom = async (value) => {
    const res = await getData(`billCommonfee/readMonthByRoom/${value}`);
    res
      ? setDate({
          Picker: [moment(res).add(1, "M") ? moment(res).add(1, "M") : undefined, undefined],
        })
      : setDate({
          Picker: [undefined, undefined],
        });
  };

  const getBuilding = async () => {
    const res = await getData("building");
    setDataBuilding(res);
  };

  const getRoom = async (e) => {
    if (e) {
      const res = await getData(`room`);
      const data = res.filter((value, index, array) => value?.building_id === e);
      form.setFieldsValue({
        com: data[0]?.commonfee_price,
        room_id: data[0]?.room_id,
      });
      changeRoom(data[0]?.room_id);
      setDataRoom(data);
    } else {
      const res = await getData(`room`);
      setDataRoom(res);
    }
  };

  const CreateCom = async (value) => {
    const resUser = await getData("user");
    const userData = resUser.filter((item) => item.user_id === localStorage.getItem("id"));
    const body = {
      room_id: value?.room_id,
      date_start: moment(value?.rangePicker[0]).format("YYYY-MM"),
      date_end: moment(value?.rangePicker[1]).format("YYYY-MM"),
      name_create: `${userData[0]?.fname} ${userData[0]?.lname} ${userData[0]?.type_name}`,
    };
    const res = await postData(`billCommonfee`, body);
    if (res?.data?.code === 201) {
      setShowModal(false);
      getDataCom();
      CreateLog("สร้างบิลค่าส่วนกลาง");
      message.success("สร้างบิลค่าส่วนกลางสำเร็จ", 2);
    } else {
      message.error("สร้างบิลค่าส่วนกลางไม่สำเร็จ", 2);
    }
  };

  const price = () => {
    setShowModal(true);
  };
  const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
  };
  const FormPrice = () => {
    return (
      <>
        <Form name="price" form={form} onFinish={CreateCom} {...layout} autoComplete="off">
          <Form.Item
            label={<span>ชื่ออาคาร</span>}
            name="building_name"
            rules={[{ required: true, message: "กรุณากรอก ชื่ออาคาร!" }]}
          >
            <Select
              allowClear
              onChange={(e) => getRoom(e)}
              disabled={localStorage.getItem("role") === "1001" ? true : false}
            >
              {dataBuilding &&
                dataBuilding?.map((item) => {
                  return (
                    <>
                      <Option value={item.building_id}>{item.building_name}</Option>
                    </>
                  );
                })}
            </Select>
          </Form.Item>
          <Form.Item
            label={<span>ชื่อห้อง</span>}
            name="room_id"
            rules={[{ required: true, message: "กรุณากรอก ชื่อห้อง!" }]}
          >
            <Select
              allowClear
              onChange={(e) => changeRoom(e)}
              disabled={localStorage.getItem("role") === "1001" ? true : false}
            >
              {dataRoom &&
                dataRoom?.map((item) => {
                  return (
                    <>
                      <Option value={item?.room_id}>{item?.room_name}</Option>
                    </>
                  );
                })}
            </Select>
          </Form.Item>
          <Form.Item
            label="ค่าส่วนกลางต่อหน่วย"
            name="com"
            rules={[{ required: true, message: "Please input your password!" }]}
          >
            <Input disabled={localStorage.getItem("role") === "1001" ? true : false} />
          </Form.Item>
          <Form.Item
            label="เลือกเดือนที่จ่าย"
            name="rangePicker"
            rules={[{ required: true, message: "Please input your password!" }]}
          >
            <RangePicker picker="month" defaultValue={date?.Picker} />
          </Form.Item>
          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <Button type="primary" htmlType="submit">
              บันทึก
            </Button>
          </Form.Item>
        </Form>
      </>
    );
  };

  return (
    <div style={{ paddingTop: "40px" }}>
      <p>จัดการค่าส่วนกลาง</p>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <div>
          <RangePicker picker="month" placeholder={["วันที่เริ่มต้น", "วันที่สิ้นสุด"]} onChange={onChangePicker} />
        </div>
        <div>
          <Button type="primary" onClick={price}>
            เก็บเงิน
          </Button>
        </div>
      </div>

      <div style={{ paddingTop: "20px" }}>
        <TablePriceCenter columns={columns} dataSource={dataCom} />
      </div>
      <ModalPrice showModal={showModal} setShowModal={setShowModal}>
        <FormPrice />
      </ModalPrice>
    </div>
  );
}
