/* eslint-disable react/jsx-pascal-case */
import React, { useEffect, useState } from "react";
import { Row, Col, Button, Modal, Form, Input, Upload, message, Image, Tooltip, Avatar, Menu, Dropdown } from "antd";
import { PlusOutlined, EditOutlined, DeleteOutlined, ExclamationCircleOutlined, BarsOutlined } from "@ant-design/icons";
import Table_news from "../../component/table/";
import ModalNews from "../../component/modal";
import { getData, postData } from "../../core/action/collection";
import { CreateLog } from "../../core/util/createlog";
import { BaseURL } from "../../baseURL";
const { confirm } = Modal;

const News = () => {
  const [form] = Form.useForm();
  const [data, setData] = useState();
  const [showModalNew, setShowModalNew] = useState(false);
  const [isType, setIsType] = useState("");
  const [newID, setNewId] = useState("");
  const [checkSizeImg, setCheckSizeImg] = useState(true);
  const [previewVisible, setPreviewVisible] = useState(false);
  const [previewImage, setPreviewImage] = useState("");
  let dataImg = [];
  const [imageUrl, setImageUrl] = useState([]);
  const [imageProfile, setImageProfile] = useState([]);

  const menu = (record) => (
    <Menu>
      <Menu.Item>
        <p
          onClick={() => {
            onEdit(record);
          }}
        >
          <span style={{ paddingRight: "10px" }}>
            <EditOutlined />
          </span>
          แก้ไข
        </p>
      </Menu.Item>
      <Menu.Item>
        <p onClick={() => deleteConfirm(record?.news_id)}>
          <span style={{ paddingRight: "10px" }}>
            <DeleteOutlined />
          </span>
          ลบ
        </p>
      </Menu.Item>
    </Menu>
  );
  const deleteConfirm = (id) => {
    confirm({
      title: "คุณต้องการลบข่าวใช่หรือไม่ ?",
      icon: <ExclamationCircleOutlined />,
      okText: "ตกลง",
      cancelText: "ยกเลิก",
      onOk() {
        onDelete(id);
      },
    });
  };

  const onDelete = async (id) => {
    const body = {
      id: id,
    };
    await postData(`/news/delete/${id}`, body);
    CreateLog("ลบข้อมูลข่าว");
    await getDataNews();
  };

  const beforeUpload = (file) => {
    const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
    const isLt2M = file.size / 1024 / 1024 < 2;

    if (!isJpgOrPng) {
      setCheckSizeImg(false);
      message.error("You can only upload JPG/PNG file!");
      return false;
    } else {
      setCheckSizeImg(true);
    }
    if (!isLt2M) {
      setCheckSizeImg(false);
      message.error("Image must smaller than 2MB!");
      return false;
    } else {
      setCheckSizeImg(true);
    }
  };

  const dummyRequest = ({ file, onSuccess }) => {
    setTimeout(() => {
      onSuccess("ok");
    }, 0);
  };

  function getBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });
  }

  const onPreview = async (file) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }
    await setPreviewImage(file.url || file.preview);
    await setPreviewVisible(true);
  };

  const handleChange = async (info) => {
    if (checkSizeImg) {
      let formData = new FormData();
      formData.append("images[]", info);
      const response = await postData("/image", formData);
      dataImg?.push({
        uid: Date.now(),
        name: response?.data?.data[0],
        status: "done",
        url: `${BaseURL?.ImageURL}${response?.data?.data[0]}`,
      });
      setImageUrl((item) => [...item, ...dataImg]);
    } else {
      return false;
    }
  };

  const handleChangeProfile = async (info) => {
    if (checkSizeImg) {
      let formData = new FormData();
      formData.append("images[]", info);
      const response = await postData("/image", formData);
      setImageProfile([
        {
          uid: Date.now(),
          name: response?.data?.data,
          status: "done",
          url: `${BaseURL?.ImageURL}${response?.data?.data[0]}`,
        },
      ]);
    } else {
      return false;
    }
  };

  const uploadButton = (
    <div>
      <PlusOutlined />
      <div style={{ marginTop: 8 }}>Upload</div>
    </div>
  );

  const colums_news = [
    {
      title: <p>ลำดับ</p>,
      width: 100,
      render: (_, data, index) => <span>{index + 1}</span>,
    },
    {
      title: <p>หัวข้อข่าว</p>,
      dataIndex: "topic",
      width: 150,
      render: (_, data, index) => (
        <div
          style={{
            maxWidth: "250px",
            textOverflow: "ellipsis",
            overflow: "hidden",
            whiteSpace: "nowrap",
            wordWrap: "break-word",
          }}
        >
          <Tooltip placement="topLeft" title={data?.content}>
            <span>{data?.topic}</span>
          </Tooltip>
        </div>
      ),
    },
    {
      title: <p>รายละเอียดข่าว</p>,
      dataIndex: "content",
      width: 150,
      render: (_, data, index) => (
        <div
          style={{
            maxWidth: "250px",
            textOverflow: "ellipsis",
            overflow: "hidden",
            whiteSpace: "nowrap",
            wordWrap: "break-word",
          }}
        >
          <Tooltip placement="topLeft" title={data?.content}>
            <span>{data?.content}</span>
          </Tooltip>
        </div>
      ),
    },
    {
      title: <p>รูปภาพปกข่าว</p>,
      dataIndex: "cover_image",
      width: 150,
      render: (data) => <Image width={100} src={`${BaseURL.ImageURL}${data}`} alt="cover_image" />,
    },
    {
      title: <p>รูปภาพ</p>,
      dataIndex: "images",
      width: 150,
      render: (data) => (
        <div>
          <Avatar.Group
            maxCount={1}
            maxPopoverTrigger="click"
            size="large"
            maxStyle={{ color: "#f56a00", backgroundColor: "#fde3cf", cursor: "pointer" }}
          >
            {data &&
              data?.split(",").map((element) => {
                return (
                  <div style={{ margin: "10" }}>
                    <Image width={100} alt="image" src={`${BaseURL.ImageURL}${element}`} />
                  </div>
                );
              })}
          </Avatar.Group>
        </div>
      ),
    },
    {
      title: <p>จัดการ</p>,
      width: 100,
      render: (_, record) => <Dropdown.Button icon={<BarsOutlined />} overlay={menu(record)} />,
    },
  ];

  const onEdit = async (record) => {
    setNewId(record?.news_id);
    setIsType("edit");
    const res = await getData("/news");
    const data = res.filter((item) => item?.news_id === record?.news_id);
    let urlProfile = [];
    let urlDetail = [];
    urlProfile.push({
      uid: new Date(),
      name: data[0]?.cover_image,
      status: "done",
      url: `${BaseURL.ImageURL}${data[0]?.cover_image}`,
    });
    if (data[0]?.images) {
      data[0]?.images.split(",").map((item, index) => {
        urlDetail.push({
          uid: index,
          name: item,
          status: "done",
          url: `${BaseURL.ImageURL}${item}`,
        });
      });
    }
    form.setFieldsValue({
      topic: record?.topic,
      content: record?.content,
    });
    setImageUrl(urlDetail);
    setImageProfile(urlProfile);
    setShowModalNew(true);
  };
  useEffect(() => {
    getDataNews();
  }, [showModalNew]);

  const getDataNews = async () => {
    const res = await getData("/news");
    const data = res?.map((item, index) => ({ ...item, key: index + 1 }));
    setData(data);
  };

  const updateNew = async (value) => {
    let formData = new FormData();
    let dataStr = [];
    imageUrl?.map((item) => {
      dataStr.push(item?.url?.split("/")[7]);
    });
    formData.append("cover_image", imageProfile[0]?.name);
    formData.append("content", value?.content);
    formData.append("topic", value?.topic);
    formData.append("images", dataStr);
    const response = await postData(`/news/${newID}`, formData);
    if (response?.data?.code === 200) {
      message.success("แก้ไขข้อมูลข่าวสำเร็จ");
      CreateLog("แก้ไขข้อมูลข่าว");
      setShowModalNew(false);
      getDataNews();
      form.resetFields();
    } else {
      form.resetFields();
      setImageUrl([]);
      message.error("แก้ไขข้อมูลข่าวไม่สำเร็จ");
    }
  };

  const onFinish = async (values) => {
    let dataStr = [];
    imageUrl?.map((item) => {
      dataStr.push(item?.url?.split("/")[7]);
    });
    let formData = new FormData();
    formData.append("cover_image", imageProfile[0]?.name[0]);
    formData.append("content", values?.content);
    formData.append("topic", values?.topic);
    formData.append("images", dataStr);
    const response = await postData("/news", formData);
    if (response?.data?.code === 201) {
      message.success("เพิ่มข้อมูลข่าวสำเร็จ");
      CreateLog("เพิ่มข้อมูลข่าว");
      setShowModalNew(false);
      getDataNews();
      form.resetFields();
    } else {
      form.resetFields();
      setImageUrl([]);
      message.error("เพิ่มข้อมูลข่าวไม่สำเร็จ");
    }
  };

  const onRemoveImgProfile = async (value) => {
    const response = await postData(`/image/delete?image_name=${value?.name}`);
    if (response?.data?.code === 200) {
      setImageProfile([]);
    }
  };

  const onRemoveImg = async (key) => {
    const response = await postData(`/image/delete?image_name=${key?.name}`);
    const data = imageUrl.filter((item) => item?.uid !== key?.uid);
    setImageUrl(data);
  };

  const openModelCreate = () => {
    setShowModalNew(true);
    setIsType("create");
    setImageUrl([]);
    setImageProfile([]);
    dataImg = [];
    form.resetFields();
  };
  return (
    <>
      <Row justify="space-between" style={{ marginBottom: "10px", paddingTop: "30px" }}>
        <Col span={12}>
          <h2>จัดการข่าวสาร</h2>
        </Col>
        <Col span={12} style={{ display: "flex", justifyContent: "end" }}>
          <Button type="primary" style={{ borderRadius: "10px", width: 120, height: 40 }} onClick={openModelCreate}>
            <PlusOutlined />
            <span>เพิ่มข่าว</span>
          </Button>
        </Col>
      </Row>
      <div>
        <Table_news columns={colums_news} dataSource={data} />
      </div>

      <ModalNews
        textTitle={isType === "create" ? "เพิ่มข่าว" : "แก้ไขข่าว"}
        showModal={showModalNew}
        setShowModal={setShowModalNew}
        width={1000}
      >
        <Form
          name="form news"
          initialValues={{ topic: "", content: "", cover_image: "" }}
          form={form}
          labelCol={{ span: 6 }}
          wrapperCol={{ span: 18 }}
          onFinish={isType === "create" ? onFinish : updateNew}
          autoComplete="off"
        >
          <Form.Item
            label={<span>รูปปกข่าวสาร</span>}
            name="cover_image"
            rules={[
              {
                required: imageProfile?.length === 1 && imageProfile ? false : true,
                message: "Please input your cover_image!",
              },
            ]}
          >
            <Upload
              name="cover_image"
              listType="picture-card"
              className="avatar-uploader"
              beforeUpload={beforeUpload}
              data={handleChangeProfile}
              customRequest={dummyRequest}
              fileList={imageProfile}
              onPreview={onPreview}
              onRemove={onRemoveImgProfile}
            >
              {imageProfile && imageProfile?.length === 1 ? null : uploadButton}
            </Upload>
          </Form.Item>
          <Form.Item
            label={<span>หัวข้อข่าว</span>}
            name="topic"
            rules={[{ required: true, message: "Please input your topic!" }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label={<span>รายละเอียดข่าว</span>}
            name="content"
            rules={[{ required: true, message: "Please input your content!" }]}
          >
            <Input.TextArea rows={3} />
          </Form.Item>
          <Form.Item
            label={
              <div>
                <p style={{ margin: "0", paddingTop: "36px" }}>รูปภาพเนื้อหา</p>
                <p style={{ fontSize: "14px" }}>เพิ่มได้สูงสุด 4 รูป</p>
              </div>
            }
            name="images"
            rules={[
              {
                required: imageUrl?.length <= 4 && imageUrl.length !== 0 ? false : true,
                message: "Please input your cover_image!",
              },
            ]}
          >
            <Upload
              name="cover_image"
              listType="picture-card"
              className="avatar-uploader"
              beforeUpload={beforeUpload}
              data={handleChange}
              customRequest={dummyRequest}
              onRemove={onRemoveImg}
              fileList={imageUrl}
              onPreview={onPreview}
            >
              {imageUrl && imageUrl?.length >= 4 ? null : uploadButton}
            </Upload>
          </Form.Item>
          <Form.Item wrapperCol={{ offset: 6, span: 18 }}>
            <div style={{ display: "flex", justifyContent: "end" }}>
              <Button style={{ borderRadius: "10px", width: 120, height: 40 }} htmlType="submit">
                <span>บันทึก</span>
              </Button>
            </div>
          </Form.Item>
        </Form>

        <Modal visible={previewVisible} onCancel={() => setPreviewVisible(false)} footer={false} title="View image">
          <img alt="example" style={{ width: "100%", height: "100%" }} src={previewImage} />
        </Modal>
      </ModalNews>
    </>
  );
};

export default News;
