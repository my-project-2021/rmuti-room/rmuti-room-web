import React, { useState, useEffect } from "react";
import TableRepair from "../../component/table/";
import { Space, Button, Input, Form, Radio, Select, Tag, Popconfirm, message } from "antd";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { getData, postData } from "../../core/action/collection";
import { manageDate } from "../../component/table/manageDate";
import ModalRepair from "../../component/modal/";
import { CreateLog } from "../../core/util/createlog";
import { Upload } from "antd";
const { Dragger } = Upload;
const { Option } = Select;
const { TextArea } = Input;
export default function Index() {
  const [data, setData] = useState();
  const [dataUser, setDataUser] = useState();
  const [dataRepairId, setDataRepairId] = useState();
  const [dataRepairDetail, setDataRepairDetail] = useState();
  const [dateId, setDateId] = useState();
  const [dataBuilding, setDataBuilding] = useState();
  const [showModal, setShowModal] = useState(false);
  const [typeFrom, setTypeFrom] = useState();
  const [repariStatus, setRepariStatus] = useState();
  const [form] = Form.useForm();

  const columns = [
    {
      title: <p>ลำดับ</p>,
      width: 150,
      render: (_, data, index) => <span>{index + 1}</span>,
    },
    {
      title: <p>รายละเอียด</p>,
      dataIndex: "repair_detail",
      width: 150,
      render: (_, data, index) => (
        <div>
          <span>{data.repair_detail}</span>
          <br></br>
          <span>{manageDate(data.repair_update_at)}</span>
          <p style={{ fontSize: "12px" }}>{data?.name_create ? `แจ้งโดย:${data?.name_create}` : ""}</p>
        </div>
      ),
    },
    {
      title: <p>การตอบกลับ</p>,
      dataIndex: "repair_respond",
      width: 150,
      render: (_, data, index) => (
        <div>
          <span>{data.repair_respond}</span>
          <br></br>
          <span>{data.repair_respond ? manageDate(data.update_at) : ""}</span>
          <p style={{ fontSize: "10px" }}>{data.name_respond ? data.name_respond : ""}</p>
        </div>
      ),
    },
    {
      title: <p>สถานะ</p>,
      dataIndex: "status_name",
      width: 150,
      render: (_, data, index) => (
        <div>
          <Tag
            color={
              data.status_name === "ดำเนินการเสร็จสิ้น"
                ? "#228B22"
                : data.status_name === "รอดำเนินการ"
                ? "#4169E1"
                : "#FF6347"
            }
          >
            {data.status_name}
          </Tag>
        </div>
      ),
    },
    {
      title: <p>จัดการ</p>,
      width: 150,
      render: (_, data, record) =>
        data.status_name === "ดำเนินการเสร็จสิ้น" ? (
          ""
        ) : data.status_name === "รอดำเนินการ" ? (
          <div style={{ display: "flex", justifyContent: "space-evenly" }}>
            <Space size="middle">
              <Button
                onClick={() => funcShowModal("updateRepari", data.repair_id, data.repair_detail)}
                icon={<EditOutlined />}
              />
            </Space>
            <Popconfirm
              placement="top"
              title={"ลบข้อมูลการแจ้งซ่อมหรือไม่ ?"}
              onConfirm={() => deleteRepari(data?.repair_id)}
              okText="ใช่"
              cancelText="ไม่"
            >
              <Button icon={<DeleteOutlined />} />
            </Popconfirm>
          </div>
        ) : (
          ""
        ),
    },
  ];

  const columnsAdmin = [
    {
      title: <p>ลำดับ</p>,
      width: 40,
      render: (_, data, index) => <span>{index + 1}</span>,
    },
    {
      title: <p>ข้อมูลห้อง</p>,
      width: 150,
      render: (_, data, index) => (
        <div>
          <span>
            {data.room_name} {data.building_name}
          </span>
          <br></br>
          <span>
            {data.fname} {data.lname}
          </span>
        </div>
      ),
    },
    {
      title: <p>รายละเอียด</p>,
      dataIndex: "repair_detail",
      width: 150,
      render: (_, data, index) => (
        <div>
          <span>{data.repair_detail}</span>
          <br></br>
          <span>{manageDate(data.update_at)}</span>
          <p style={{ fontSize: "12px" }}>{data?.name_create ? `แจ้งโดย:${data?.name_create}` : ""}</p>
        </div>
      ),
    },
    {
      title: <p>การตอบกลับ</p>,
      dataIndex: "repair_respond",
      width: 150,
      render: (_, data, index) => (
        <div>
          <span>{data.repair_respond}</span>
          <br></br>
          <span>{data.repair_respond ? manageDate(data.update_at) : ""}</span>
          <p style={{ fontSize: "12px" }}>{data?.name_respond ? data?.name_respond : ""}</p>
        </div>
      ),
    },
    {
      title: <p>สถานะ</p>,
      dataIndex: "status_name",
      width: 150,
      render: (_, data, index) => (
        <div>
          <Tag
            color={
              data.status_name === "ดำเนินการเสร็จสิ้น"
                ? "#228B22"
                : data.status_name === "รอดำเนินการ"
                ? "#4169E1"
                : "#FF6347"
            }
          >
            {data.status_name}
          </Tag>
        </div>
      ),
    },
    {
      title: <p>จัดการ</p>,
      width: 150,
      render: (_, data, record) =>
        data?.status_name === "ดำเนินการเสร็จสิ้น" ? (
          ""
        ) : (
          <Space size="middle">
            {data?.user_id === localStorage.getItem("id") ? (
              data?.status_name !== "ดำเนินการเสร็จสิ้น" && (
                <Button
                  type="primary"
                  onClick={() => funcShowModal("updateRepari", data.repair_id, data.repair_detail)}
                  style={{ borderRadius: "10px", width: 120, height: 40 }}
                >
                  <span>แก้ไข</span>
                </Button>
              )
            ) : (
              <Button
                type="primary"
                onClick={() =>
                  funcShowModal(
                    "respond",
                    data?.repair_id,
                    data?.repair_detail,
                    data?.status_name,
                    data?.respond_detail,
                    data?.repair_respond
                  )
                }
                style={{ borderRadius: "10px", width: 120, height: 40 }}
              >
                <span>ตอบกลับ</span>
              </Button>
            )}
          </Space>
        ),
    },
  ];

  useEffect(() => {
    getDataUser();
    if (localStorage.getItem("role") === "1001") {
      getDataRepairById();
    } else {
      getDataRepair();
    }
    getBuilding();
  }, []);

  const getDataUser = async () => {
    const res = await getData(`/user/${localStorage.getItem("id")}`);
    setDataUser(res);
  };

  const deleteRepari = async (id) => {
    const res = await postData(`/repair/delete/${id}`);
    message.open({
      type: "success",
      content: "ลบข้อมูลสำเร็จ",
    });
    await CreateLog("ลบการแจ้งซ่อม");
    await getDataRepairById();
  };

  const getBuilding = async () => {
    const res = await getData("building");
    setDataBuilding(res);
  };

  const funcShowModal = (type, repair_id, repair_detail, status, respond_detail, repair_respond) => {
    setShowModal(true);
    setRepariStatus(status === "รอดำเนินการ" ? 1 : status === "กำลังดำเนินการ" ? 2 : 3);
    setTypeFrom(type);
    setDataRepairId(repair_id);
    form.setFieldsValue({
      detail: repair_detail,
      respond_detail: respond_detail,
      repairRespond: repair_respond,
    });
  };

  const FromInsertRepair = () => {
    form.resetFields();
    return (
      <>
        <h3>รายละเอียด</h3>
        <Form form={form} onFinish={CreateRepairData} name="insert" initialValues={{ remember: true }}>
          <Form.Item name="detail" rules={[{ required: true, message: "Please input your detail!" }]}>
            <TextArea placeholder="รายละเอียด" autoSize={{ minRows: 3, maxRows: 5 }} />
          </Form.Item>
          <Form.Item>
            <div style={{ display: "flex", justifyContent: "end" }}>
              <Button type="primary" htmlType="submit" style={{ borderRadius: "10px" }}>
                <span>บันทึก</span>
              </Button>
            </div>
          </Form.Item>
        </Form>
      </>
    );
  };

  const FormRespondRepair = () => {
    return (
      <>
        <h3>รายละเอียด</h3>
        <Form form={form} name="respond" onFinish={UpdateRepairData} initialValues={{ remember: true }}>
          <Form.Item name="detail" rules={[{ required: true, message: "Please input your detail!" }]}>
            <TextArea disabled placeholder="รายละเอียด" autoSize={{ minRows: 3, maxRows: 5 }} />
          </Form.Item>
          <h3>ตอบกลับ</h3>
          <Form.Item name="repairRespond" rules={[{ required: true, message: "Please input your respond!" }]}>
            <TextArea placeholder="ตอบกลับ" autoSize={{ minRows: 3, maxRows: 5 }} />
          </Form.Item>
          <Form.Item
            name="respond_status"
            rules={[{ required: repariStatus ? false : true, message: "Please input your status!" }]}
          >
            <Radio.Group name="radiogroup" onChange={(e) => setRepariStatus(e)} defaultValue={repariStatus}>
              <Radio value={1}>รอดำเนินการ</Radio>
              <Radio value={2}>กำลังดำเนินการ</Radio>
              <Radio value={3}>ดำเนินการเสร็จสิ้น</Radio>
            </Radio.Group>
          </Form.Item>
          <Form.Item>
            <div style={{ display: "flex", justifyContent: "end" }}>
              <Button type="primary" htmlType="submit" style={{ borderRadius: "10px" }}>
                <span>บันทึก</span>
              </Button>
            </div>
          </Form.Item>
        </Form>
      </>
    );
  };

  const FromUpdateRepari = () => {
    return (
      <>
        <h3>รายละเอียด</h3>
        <Form form={form} onFinish={UpdateRepairData} name="update" initialValues={{ remember: true }}>
          <Form.Item name="detail" rules={[{ required: true, message: "Please input your detail!" }]}>
            <TextArea placeholder="รายละเอียด" autoSize={{ minRows: 3, maxRows: 5 }} />
          </Form.Item>
          <Form.Item>
            <div style={{ display: "flex", justifyContent: "end" }}>
              <Button type="primary" htmlType="submit" style={{ borderRadius: "10px" }}>
                <span>บันทึก</span>
              </Button>
            </div>
          </Form.Item>
        </Form>
      </>
    );
  };

  const getDataRepairById = async () => {
    const res = await getData(
      `/repair/getRepairData?user_id=${localStorage.getItem("id")}&role=${localStorage.getItem("role")}`
    );
    const dataRepair = res?.map((item, index) => ({
      ...item,
      key: index + 1,
    }));
    setData(dataRepair);
  };

  const getDataRepair = async (id, type) => {
    const res = await getData(`/repair/getRepairData`);
    const dataRepair = res?.map((item, index) => ({
      ...item,
      key: index + 1,
    }));
    const resUser = await getData(`/booking`);
    const dataUser = resUser.filter((value, index, array) => value?.user_id === localStorage.getItem("id"));
    const resBulding = await getData(`/building`);
    const dataBuildingFilter = resBulding?.filter(
      (value, index, array) => value?.building_id === dataUser[0]?.building_id
    );

    if (localStorage.getItem("role") === "1003") {
      const dataFilter = await dataRepair?.filter((value, index, array) => value?.building_id === (id ? id : dateId));
      await setData(type || dateId ? dataFilter : dataRepair);
    } else {
      const dataFilter = await dataRepair?.filter(
        (value, index, array) => value?.building_id === dataBuildingFilter[0]?.building_id
      );
      await setData(dataFilter);
    }
  };

  const UpdateRepairData = async (value) => {
    const res = await getData("user");
    const userData = res.filter((item) => item.user_id === localStorage.getItem("id"));
    const body = {
      repair_id: dataRepairId,
      repair_detail: value.detail,
      repair_respond: value?.repairRespond,
      repair_status_id: repariStatus?.target?.value ? repariStatus?.target?.value : "1",
      name_respond: `${userData[0]?.fname} ${userData[0]?.lname} ${userData[0]?.type_name}`,
    };
    await postData(`/repair/UpdateRepair`, { ...body });
    if (localStorage.getItem("role") === "1001") {
      await getDataRepairById();
    } else {
      await getDataRepair();
    }
    await CreateLog("ตอบกลับการแจ้งซ่อม");
    setShowModal(false);
    form.resetFields();
  };

  const CreateRepairData = async (value) => {
    const resUser = await getData("user");
    const userData = resUser?.filter((item) => item.user_id === localStorage.getItem("id"));
    const body = {
      user_id: localStorage.getItem("id"),
      repair_detail: value.detail,
      repair_status_id: 1,
      name_create: `${userData[0]?.fname} ${userData[0]?.lname} ${userData[0]?.type_name}`,
    };
    await postData(`/repair/CreateRepair`, { ...body });
    if (localStorage.getItem("role") === "1001") {
      await getDataRepairById();
      CreateLog("สร้างการแจ้งซ่อม");
    } else {
      CreateLog("สร้างการแจ้งซ่อม");
      await getDataRepair();
    }
    setShowModal(false);
    form.resetFields();
  };

  const dateSelect = async (id) => {
    if (id) {
      await setDateId(id);
      await getDataRepair(id, "search");
    } else {
      await setDateId();
      await getDataRepair();
    }
  };

  return (
    <div>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <h2>การแจ้งซ่อม</h2>
        <div
          style={{
            display: localStorage.getItem("role") !== "1003" ? "block" : "none",
            paddingTop: "40px",
          }}
        >
          <Button
            type="primary"
            onClick={() => funcShowModal("insertRepair")}
            style={{
              borderRadius: "10px",
            }}
          >
            <span> แจ้งซ่อม</span>
          </Button>
        </div>
      </div>
      <div
        style={{
          paddingBottom: "10px",
          display: localStorage.getItem("role") !== "1003" ? "block" : "none",
        }}
      >
        <span style={{ paddingRight: "20px", fontSize: "18px" }}>
          ชื่อผู้พักอาศัย: {dataUser && dataUser?.fname} {dataUser && dataUser?.lname}
        </span>
        <span style={{ fontSize: "18px" }}>
          {dataUser && dataUser?.building_name} หมายเลขห้อง: {dataUser && dataUser?.room_name}
        </span>
      </div>
      <div style={{ display: localStorage.getItem("role") === "1003" ? "block" : "none" }}>
        <p>เลือกอาคาร</p>
        <Select style={{ width: 120 }} onChange={(id) => dateSelect(id)} allowClear>
          {dataBuilding?.map((item, index) => {
            return (
              <Option key={`Expemses${index}`} value={item?.building_id}>
                {item?.building_name}
              </Option>
            );
          })}
        </Select>
      </div>

      <TableRepair
        columns={
          localStorage.getItem("role") === "1003" || localStorage.getItem("role") === "1002" ? columnsAdmin : columns
        }
        dataSource={data}
      />

      <ModalRepair
        showModal={showModal}
        typeModal="Insert"
        textModal="แจ้งซ่อม"
        textTitle="การแจ้งซ่อม"
        setDataRepairId={setDataRepairId}
        setDataRepairDetail={setDataRepairDetail}
        setShowModal={setShowModal}
      >
        {typeFrom === "updateRepari" ? (
          <FromUpdateRepari />
        ) : typeFrom === "insertRepair" ? (
          <FromInsertRepair />
        ) : (
          <FormRespondRepair />
        )}
      </ModalRepair>
    </div>
  );
}
