import React, { useEffect, useState } from "react";
import { getData, postData, putData } from "../../core/action/collection";
import { Form, Input, Button, InputNumber, Select, DatePicker, Modal } from "antd";
import moment from "moment";
import { useNavigate, useParams } from "react-router-dom";
import { createBrowserHistory } from "history";
import { BaseURL } from "../../baseURL";

const history = createBrowserHistory();
export default function Index(props) {
  const [dataBuilding, setDataBuilding] = useState();
  const navigate = useNavigate();
  const location = window.location.pathname.split("/");
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [dataRoom, setDataRoom] = useState();
  const [userType, setUserType] = useState();
  const [type, setType] = useState();
  const { Option } = Select;
  const [form] = Form.useForm();

  useEffect(() => {
    getDataUser();
    getBuilding();
    getRoomData();
    getUserType();
  }, []);

  const getDataUser = async () => {
    const res = await getData(`/user/${location[location.length - 1]}`);
    if (res?.user_status === "1") {
      localStorage.setItem("status_id", res?.status_id);
      navigate("About");
    } else {
      form.setFieldsValue({
        Fname: res.fname,
        Lname: res.lname,
        IDcard: res.identify_number,
      });
    }
  };

  const getUserType = async () => {
    const res = await getData(`/user_type`);
    setUserType(res);
  };

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const getBuilding = async () => {
    const res = await getData("building");
    setDataBuilding(res);
  };

  const getRoomData = async (value) => {
    const res = await getData("/room");
    const data = res.filter((item, index, array) => item.building_id === value && item.room_status === "0");
    setDataRoom(data);
  };

  const onChangeBuilding = (value) => {
    getRoomData(value);
  };

  const deleteUser = async (data) => {
    const res = await postData(`user/delete/${location[location.length - 1]}`);
    setIsModalOpen(false);
    await localStorage.clear();
    await window.location.replace(BaseURL.logoutSSO);
  };

  const onFinish = async (values) => {
    const body = {
      identify_number: values?.IDcard,
      fname: values?.Fname,
      lname: values?.Lname,
      tel: values?.Tel,
      type_id: values?.type_id,
      status_id: "1",
    };

    const body_booking = {
      check_in: moment(values?.Check_in?._d).format("YYYY-MM-DD"),
      check_out: moment(values?.Check_out?._d).format("YYYY-MM-DD"),
      room_id: values?.RoomNumber,
      user_id: location[location.length - 1],
    };

    await postData(`/user/${location[location.length - 1]}`, body);
    if (type !== "1003") {
      await postData(`booking`, body_booking);
      const test = dataRoom.filter((item) => item.room_id === values.RoomNumber);
      const bodyRoom = {
        building_id: test[0].building_id,
        room_name: test[0].room_name,
        room_status: 1,
        water_unit: test[0].water_unit,
        power_unit: test[0].power_unit,
      };
      await postData(`/room/${values?.RoomNumber}`, { ...bodyRoom });
    }
    window.location.replace("/pws/web/waiting");
  };

  return (
    <div style={{ padding: "10% 10% 10% 10%" }}>
      <div>
        <p style={{ textAlign: "center" }}>สมัครสมาชิก</p>
      </div>

      <div>
        <Form
          form={form}
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 12 }}
          initialValues={{ remember: true }}
          onFinish={onFinish}
          style={{ paddingTop: "20px" }}
          autoComplete="off"
        >
          <Form.Item label={<span>ชื่อ</span>} name="Fname">
            <Input disabled />
          </Form.Item>
          <Form.Item label={<span>นามสกุล</span>} name="Lname">
            <Input disabled />
          </Form.Item>
          <Form.Item label={<span>เลขบัตรประชาชน</span>} name="IDcard">
            <Input disabled />
          </Form.Item>

          <Form.Item
            label={<span>เบอร์โทรศัพท์</span>}
            name="Tel"
            rules={[
              {
                required: true,
                message: "Please input your value.",
              },
              {
                pattern: /^[0-9]+$/,
                message: "Name can only include letters and numbers.",
              },
            ]}
          >
            <Input placeholder="เบอร์โทรศัพท์" />
          </Form.Item>
          <Form.Item
            label={<span>ประเภทผู้ใช้งาน</span>}
            name="type_id"
            rules={[{ required: true, message: "Please input your value!" }]}
          >
            <Select
              showSearch
              style={{ width: 300 }}
              placeholder="ประเภทผู้ใช้งาน"
              onChange={(e) => setType(e)}
              optionFilterProp="children"
              filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
            >
              {userType &&
                userType.map((item) => {
                  return <Option value={item.type_id}>{item.type_name}</Option>;
                })}
            </Select>
          </Form.Item>
          {type !== "1003" && (
            <div>
              <Form.Item
                label={<span>ชื่ออาคาร</span>}
                name="BuildingName"
                rules={[{ required: true, message: "Please input your value!" }]}
              >
                <Select
                  showSearch
                  style={{ width: 300 }}
                  placeholder="ชื่ออาคาร"
                  onChange={onChangeBuilding}
                  optionFilterProp="children"
                  filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                  filterSort={(optionA, optionB) =>
                    optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
                  }
                >
                  {dataBuilding &&
                    dataBuilding.map((item) => {
                      return <Option value={item.building_id}>{item.building_name}</Option>;
                    })}
                </Select>
              </Form.Item>

              <Form.Item
                label={<span>หมายเลขห้อง</span>}
                name="RoomNumber"
                rules={[{ required: true, message: "Please input your value!" }]}
              >
                <Select
                  showSearch
                  style={{ width: 300 }}
                  placeholder="หมายเลขห้อง"
                  optionFilterProp="children"
                  filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                  filterSort={(optionA, optionB) =>
                    optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
                  }
                >
                  {dataRoom &&
                    dataRoom.map((item) => {
                      return <Option value={item.room_id}>{item.room_name}</Option>;
                    })}
                </Select>
              </Form.Item>

              <Form.Item
                label={<span>วันเข้าอยู่</span>}
                name="Check_in"
                rules={[{ required: true, message: "Please input your value!" }]}
              >
                <DatePicker />
              </Form.Item>
              <Form.Item
                label={<span>วันออก</span>}
                name="Check_out"
                rules={[{ required: true, message: "Please input your value!" }]}
              >
                <DatePicker />
              </Form.Item>
            </div>
          )}

          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <div style={{ display: "flex", justifyContent: "space-evenly" }}>
              <Button type="primary" htmlType="submit">
                บันทึก
              </Button>
              <Button htmlType="button" onClick={showModal}>
                ยกเลิก
              </Button>
            </div>
          </Form.Item>

          <Form.Item wrapperCol={{ offset: 8, span: 16 }}></Form.Item>
        </Form>
        <Modal visible={isModalOpen} onOk={deleteUser} onCancel={handleCancel}>
          <h3>คุณต้องการยกเลิกการสมัครสมาชิกหรือไม่ ?</h3>
        </Modal>
      </div>
    </div>
  );
}
