import React from "react";
import { Form, Input, Button, Checkbox } from "antd";
import { useFeature } from "../../core/store/";

const LoginPage = (props) => {
  const { setIsLogin } = useFeature();
  const onFinish = async (values) => {
    if (values.username === "admin" && values.password === "admin") {
      localStorage.setItem("User_id", "5");
      localStorage.setItem("Login", true);
      localStorage.setItem("role", "1003");
      setIsLogin(true);
      window.location.reload();
    } else if (values.username === "user1" && values.password === "user1") {
      localStorage.setItem("User_id", "3");
      localStorage.setItem("Login", true);
      localStorage.setItem("role", "1002");
      setIsLogin(true);
      window.location.reload();
    } else if (values.username === "user2" && values.password === "user2") {
      localStorage.setItem("Login", true);
      localStorage.setItem("User_id", "4");
      localStorage.setItem("role", "1001");
      setIsLogin(true);
      window.location.reload();
    }
  };
  const onFinishFailed = () => {
    console.log("Failed:");
  };

  return (
    <div className="login">
      <div className="login-container">
        <span className="title-login">Login</span>
        <div style={{ marginTop: "20px" }}>
          <Form
            name="basic"
            initialValues={{ remember: true }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            layout="vertical"
          >
            <Form.Item
              style={{ textAlign: "left" }}
              label="Username"
              name="username"
              rules={[{ required: true, message: "Please input your username!" }]}
            >
              <Input style={{ borderRadius: "10px" }} />
            </Form.Item>

            <Form.Item
              style={{ textAlign: "left" }}
              label="Password"
              name="password"
              rules={[{ required: true, message: "Please input your password!" }]}
            >
              <Input.Password style={{ borderRadius: "10px" }} />
            </Form.Item>

            <Form.Item style={{ textAlign: "left" }} name="remember" valuePropName="checked">
              <Checkbox>Remember me</Checkbox>
            </Form.Item>

            <Form.Item>
              <Button
                style={{
                  borderRadius: "20px",
                  width: "200px",
                  fontWeight: "bold",
                }}
                type="primary"
                htmlType="submit"
              >
                Submit
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
};

export default LoginPage;
