// import React, { useState, useEffect } from "react";
// import TableUser from "../../component/table/";
// import { getData, postData } from "../../core/action/collection";
// import ModalBooking from "../../component/modal/";
// import { Button, Input, Form, Select, DatePicker, message, Dropdown, Menu, Popconfirm } from "antd";
// import { manageDate } from "../../component/table/manageDate";
// import { BarsOutlined } from "@ant-design/icons";
// import moment from "moment";
// import styled from "styled-components";
// const { Option } = Select;
// export default function Index() {
//   const [dataBooking, setDataBooking] = useState();
//   const [showModal, setShowModal] = useState(false);
//   const [bookingId, setBookingId] = useState();
//   const [dataRoom, setDataRoom] = useState();
//   const [timeBooking, setTimeBooking] = useState({
//     check_in: "",
//     check_out: "",
//   });

//   const [form] = Form.useForm();

//   const menu = (data) => (
//     <Menu>
//       <Menu.Item>
//         <p onClick={() => updateBooking(data)}> แก้ไข</p>
//       </Menu.Item>
//       <Menu.Item>
//         <BoxPopupConfirm
//           className="popup-booking"
//           title="หากลบข้อมูลออกไปแล้วจะไม่สามารถ เข้าใช้งานได้จนกว่าจะทำงานจองและ อนุมัติใหม่อีกครั้ง"
//           okText="ตกลง"
//           onConfirm={() => deleteBppking(data)}
//           cancelText="ยกเลิก"
//         >
//           <p>ลบ</p>
//         </BoxPopupConfirm>
//       </Menu.Item>
//       <Menu.Item>
//         <p onClick={() => confirmBooking(data)}> {data.booking_status === "0" ? "อนุมัติ" : "ไม่อนุมัติ"}</p>
//       </Menu.Item>
//     </Menu>
//   );

//   const columns = [
//     {
//       title: <p>ลำดับ</p>,
//       width: 50,
//       render: (_, data, index) => <div>{index + 1}</div>,
//     },
//     {
//       title: <p>ข้อมูลผู้ใช้</p>,
//       width: 150,
//       render: (_, data) => (
//         <div>
//           <p>ชื่อ: {data.fname}</p>
//           <p>นามสกุล:{data.lname}</p>
//           <p>เบอร์โทร:{data.tel}</p>
//         </div>
//       ),
//     },
//     {
//       title: <p>ข้อมูลห้อง</p>,
//       width: 150,
//       key: "",
//       render: (_, data) => (
//         <div>
//           <p>ชื่อตึก: {data.building_name ? data.building_name : "-"}</p>
//           <p>หมายเลขห้อง: {data.room_name ? data.room_name : "-"}</p>
//         </div>
//       ),
//     },
//     {
//       title: <p>วันเข้าอยู่</p>,
//       width: 150,
//       render: (_, data, index) => <p>{manageDate(data.check_in)}</p>,
//     },
//     {
//       title: <p>วันออก</p>,
//       width: 150,
//       render: (_, data) => <p>{manageDate(data.check_out)}</p>,
//     },
//     {
//       title: <p>สถานะ</p>,
//       dataIndex: "response_detail",
//       width: 150,
//       render: (_, data) => (
//         <div>
//           <span>{data.booking_status === "0" ? "รออนุมัติ" : "อนุมัติเรียบร้อย"}</span>
//         </div>
//       ),
//     },
//     {
//       title: <p>สิทธิ์การเข้าใช้</p>,
//       dataIndex: "type_name",
//       width: 150,
//       render: (_, data, index) => <p>{data?.type_name}</p>,
//     },
//     {
//       title: <p>จัดการ</p>,
//       width: 150,
//       render: (_, data) => <Dropdown.Button icon={<BarsOutlined />} overlay={menu(data)} />,
//     },
//   ];

//   useEffect(() => {
//     getDataBooking();
//     getRoom();
//   }, []);

//   const getRoom = async () => {
//     const res = await getData(`room`);
//     setDataRoom(res);
//   };

//   const updateBooking = async (data) => {
//     setBookingId(data.booking_id);
//     setShowModal(true);
//     getRoom();
//     form.setFieldsValue({
//       fname: data.fname,
//       lname: data.lname,
//       building_name: data.building_id,
//       room_id: data.room_id,
//       check_in: moment(data.check_in, "YYYY-MM-DD"),
//       check_out: moment(data.check_out, "YYYY-MM-DD"),
//     });
//   };

//   const deleteBppking = async (data) => {
//     const res = await postData(`booking/delete/${data.booking_id}`);
//     message.success("ลบข้อมูลสำเร็จ");
//     getDataBooking();
//   };

//   const getDataBooking = async () => {
//     const res = await getData("booking");
//     const data = res?.map((item, index) => ({ ...item, key: index + 1 }));
//     setDataBooking(data);
//   };

//   const confirmBooking = async (data) => {
//     const body = {
//       booking_status: data.booking_status === "0" ? "1" : "0",
//     };
//     console.log("body :>> ", body);

//     const res = await postData(`booking/update_status/${data.booking_id}`, body);
//     console.log("res :>> ", res);
//     if (res?.data?.code === 200) {
//       message.success("เปลี่ยนคำร้องขอเรียบร้อย");
//       getDataBooking();
//     } else {
//       // message.error("ห้องนี้ถูกอนุมัติไปแล้ว โปรดเลือกห้องอื่น");
//     }
//   };

//   const onUpdateBooking = async (value) => {
//     const body = {
//       check_in: moment(value.check_in._d).format("YYYY-MM-DD"),
//       check_out: moment(value.check_out._d).format("YYYY-MM-DD"),
//       room_id: value.room_id,
//     };
//     const res = await postData(`booking/${bookingId}`, body);
//     if (res.data.code === 200) {
//       message.success("แก้ไขเรียบร้อย");
//       getDataBooking();
//       setShowModal(false);
//     } else {
//       message.error("ห้องนี้มีผู้พักแล้ว");
//       setShowModal(false);
//     }
//   };

//   const layout = {
//     labelCol: { span: 8 },
//     wrapperCol: { span: 16 },
//   };

//   const FormUpdateBooking = () => {
//     return (
//       <>
//         <Form name="updateBooking" form={form} {...layout} autoComplete="off" onFinish={onUpdateBooking}>
//           <Form.Item label={<span>ชื่อ</span>} name="fname">
//             <Input disabled />
//           </Form.Item>
//           <Form.Item label={<span>นามสกุล</span>} name="lname">
//             <Input disabled />
//           </Form.Item>

//           <Form.Item
//             label={<span>ชื่อห้อง</span>}
//             name="room_id"
//             rules={[{ required: true, message: "กรุณากรอก ชื่อห้อง!" }]}
//           >
//             <Select allowClear>
//               {dataRoom &&
//                 dataRoom.map((item) => {
//                   return (
//                     <>
//                       <Option value={item.room_id}>{item.room_name}</Option>
//                     </>
//                   );
//                 })}
//             </Select>
//           </Form.Item>
//           <Form.Item label={<span>วันที่เข้าอยู่</span>} name="check_in">
//             <DatePicker />
//           </Form.Item>
//           <Form.Item label={<span>วันที่ออก</span>} name="check_out">
//             <DatePicker />
//           </Form.Item>
//           <div style={{ display: "flex", justifyContent: "end" }}>
//             <Form.Item>
//               <Button style={{ borderRadius: "10px", width: 120, height: 40 }} type="primary" htmlType="submit">
//                 <span>บันทึก</span>
//               </Button>
//             </Form.Item>
//           </div>
//         </Form>
//       </>
//     );
//   };
//   return (
//     <div>
//       <h2>จัดการข้อมูลผู้พักอาศัย</h2>
//       <TableUser columns={columns} dataSource={dataBooking} />
//       <ModalBooking showModal={showModal} setShowModal={setShowModal} textTitle="จัดการผู้ใช้">
//         <FormUpdateBooking />
//       </ModalBooking>
//     </div>
//   );
// }

// const BoxPopupConfirm = styled(Popconfirm)`
//   .popup-booking {
//     .ant-popover-buttons button {
//       margin-left: 8px;
//       height: 35px !important;
//     }
//   }
// `;
