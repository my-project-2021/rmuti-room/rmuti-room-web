/* eslint-disable react-hooks/exhaustive-deps */
import { useParams } from "react-router-dom";
import React, { useEffect, useState } from "react";
import { Table, InputNumber, Button, Modal, Form, Input, Row, Col, Select, Menu, Dropdown, message } from "antd";
import {
  PlusOutlined,
  ExclamationCircleOutlined,
  EditOutlined,
  DeleteOutlined,
  EyeOutlined,
  BarsOutlined,
} from "@ant-design/icons";
import { getData, postData } from "../../core/action/collection";
import ModalRepair from "../../component/modal/";
import { useNavigate } from "react-router-dom";
import { CreateLog } from "../../core/util/createlog";
const { confirm } = Modal;
const { Option } = Select;
const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};
export default function Index() {
  const { id } = useParams();
  const [showModal, setShowModal] = useState(false);
  const [dataBuilding, setDataBuilding] = useState();
  const [dataRoom, setDataRoom] = useState();
  const [typeFrom, setTypeFrom] = useState();
  const [roomId, setRoomId] = useState();
  const [form] = Form.useForm();
  const navigate = useNavigate();

  const menu = (data) => (
    <Menu>
      <Menu.Item>
        <p onClick={() => setValueRoom(data)}>
          <span style={{ paddingRight: "10px" }}>
            <EditOutlined />
          </span>
          แก้ไข
        </p>
      </Menu.Item>
      <Menu.Item>
        <p onClick={() => showDeleteConfirm(data.room_id, data.room_name, "Room")}>
          <span style={{ paddingRight: "10px" }}>
            <DeleteOutlined />
          </span>
          ลบ
        </p>
      </Menu.Item>
      {data.room_status === "1" && (
        <Menu.Item>
          <p onClick={() => navigate(`view-user/${data?.room_id}`)}>
            <span style={{ paddingRight: "10px" }}>
              <EyeOutlined />
            </span>
            ดูข้อมูล
          </p>
        </Menu.Item>
      )}
    </Menu>
  );

  const columnsRoom = [
    {
      title: <p>#</p>,
      width: "15%",
      render: (_, data, index) => <span>{index + 1}</span>,
    },
    {
      title: <p>หมายเลขห้อง</p>,
      dataIndex: "room_name",
      key: "room_name",
      render: (_, data, index) => <span>{data?.room_name}</span>,
    },
    {
      title: <p>ชื่ออาคาร</p>,
      dataIndex: "building_name",
      key: "building_name",
      render: (_, data, index) => <span>{data?.building_name}</span>,
    },
    {
      title: <p>มิเตอร์น้ำ</p>,
      dataIndex: "water_unit",
      key: "water_unit",
      render: (_, data, index) => <span>{data?.water_unit}</span>,
    },
    {
      title: <p>มิเตอร์ไฟ</p>,
      dataIndex: "power_unit",
      key: "power_unit",
      render: (_, data, index) => <span>{data?.power_unit}</span>,
    },
    {
      title: <p>สถานะ</p>,
      dataIndex: "room_status",
      key: "room_status",
      render: (_, data) => {
        return data.room_status === "0" ? (
          <span style={{ color: "#FAD02C" }}>ว่าง</span>
        ) : (
          <span style={{ color: "#f05c5c" }}>ไม่ว่าง</span>
        );
      },
    },
    {
      title: <p>จัดการ</p>,
      width: "10%",
      render: (_, data) => <Dropdown.Button icon={<BarsOutlined />} overlay={menu(data)} />,
    },
  ];

  useEffect(() => {
    getRoomData();
    getBuilding();
  }, []);

  const getRoomData = async () => {
    const res = await getData("room", id);
    const result = res?.filter((item) => item?.building_id === id);
    const data = result?.map((item, index) => ({
      ...item,
      key: index + 1,
    }));
    setDataRoom(data);
    console.log("data", data);
    const resbooking = await getData("/booking");
    console.log("resbooking", resbooking);
    const resultbooking = resbooking?.filter((item) => item?.room_id === id);
    console.log("id", id);
    console.log("resultbooking", resultbooking);
    // data.map(async (item) => {
    //   if (item?.room_id !== resultbooking?.room_id) {
    //     const bodyRoom = {
    //       building_id: item.building_id,
    //       room_name: item.room_name,
    //       room_status: 0,
    //       water_unit: item.water_unit,
    //       power_unit: item.power_unit,
    //     };
    //     console.log("bodyRoom", bodyRoom);
    //     const resp = await postData(`/room/${item?.room_id}`, { ...bodyRoom });
    //     console.log("resp", resp);
    //   }
    // });
  };

  const getBuilding = async () => {
    const res = await getData("building");
    const data = res.map((item, index) => ({
      ...item,
      key: index + 1,
    }));
    setDataBuilding(data);
  };
  const showModalFrom = (type) => {
    if (type === "CreateRoom") {
      setTypeFrom(type);
      setShowModal(true);
    } else {
      setTypeFrom(type);
      setShowModal(true);
    }
  };

  const setValueRoom = (data) => {
    setRoomId(data.room_id);
    showModalFrom("UpdateRoom");
    form.setFieldsValue({
      buildingName: data.building_id,
      numberRoom: data.room_name,
      statusRoom: data.room_status,
      water_unit: data.water_unit,
      power_unit: data.power_unit,
      // room_price: data.room_price,
    });
  };

  const DeleteRoom = async (value) => {
    const res = await postData(`/room/delete/${value}`);
    if (res?.data?.code !== 200) {
      message.error("ไม่สามารถลบอาคารได้เนื่องจากมีผู้พักอาศัยอยู่");
    } else {
      await getBuilding();
      await CreateLog("ลบข้อมูลห้อง");
    }
    await getRoomData();
  };

  const showDeleteConfirm = (id, name, type) => {
    confirm({
      title: `คุณต้องการลบ ${name && name} ใช่หรือไม่ ?`,
      icon: <ExclamationCircleOutlined />,
      cancelText: "ยกเลิก",
      okText: "ตกลง",
      onOk() {
        DeleteRoom(id);
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  };
  const onFinish = async (value) => {
    const checkName = dataRoom?.filter((item) => {
      value.numberRoom.replace(" ", "").match(item.room_name.replace(" ", ""));
    });
    if (checkName?.length > 0) {
      message.error("ไม่สามรถเพิ่มห้องซ้ำกันได้");
    } else {
      const body = {
        building_id: id,
        room_name: value.numberRoom,
        // room_price: value.room_price,
        room_status: value.statusRoom,
        water_unit: value.water_unit,
        power_unit: value.power_unit,
      };
      if (typeFrom === "UpdateRoom") {
        await postData(`/room/${roomId}`, { ...body });
        await getRoomData();
        await CreateLog("อัพเดทข้อมูลห้อง");

        setShowModal(false);
      } else {
        await postData(`/room`, { ...body });
        await getRoomData();
        await CreateLog("สร้างข้อมูลห้อง");
        await setShowModal(false);
        form.resetFields();
      }
    }
  };

  const FromRoom = () => {
    if (typeFrom === "CreateRoom") {
      form.resetFields();
    }
    return (
      <>
        <h2>รายละเอียด</h2>
        <Form form={form} onFinish={onFinish} onFinishFailed={onFinish} {...layout}>
          <Form.Item label={<p>ชื่ออาคาร</p>} name="buildingName">
            <Select
              showSearch
              style={{ width: 300 }}
              placeholder="ชื่ออาคาร"
              optionFilterProp="children"
              defaultValue={id}
              filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
            >
              {dataBuilding &&
                dataBuilding.map((item, index) => {
                  return (
                    item.building_id === id && (
                      <Option key={index + 1} value={item.building_id}>
                        {item.building_name}
                      </Option>
                    )
                  );
                })}
            </Select>
          </Form.Item>
          <Form.Item
            label={<p>หมายเลขห้อง</p>}
            name="numberRoom"
            rules={[{ required: true, message: "กรุณากรอก หมายเลขห้อง!" }]}
          >
            <Input style={{ width: 300 }} placeholder="หมายเลขห้อง" />
          </Form.Item>
          <Form.Item
            label={<p>หน่วยค่าน้ำปัจจุบัน</p>}
            name="water_unit"
            rules={[{ required: true, message: "กรุณากรอก หน่วยค่าน้ำปัจจุบัน!" }]}
          >
            <InputNumber style={{ width: 300 }} min={0} placeholder="ค่าส่วนกลาง (กรอกตัวเลข)" />
          </Form.Item>
          <Form.Item
            label={<p>หน่วยค่าไฟปัจจุบัน</p>}
            name="power_unit"
            rules={[{ required: true, message: "กรุณากรอก หน่วยค่าไฟปัจจุบัน!" }]}
          >
            <InputNumber style={{ width: 300 }} min={0} placeholder="หน่วยค่าไฟปัจจุบัน (กรอกตัวเลข)" />
          </Form.Item>
          <div style={{ display: "none" }}>
            <Form.Item label={<p>สถานะห้อง</p>} name="statusRoom">
              <Select style={{ width: 120 }}>
                <Option value={0}>ว่าง</Option>
                <Option value={1}>ไม่ว่าง</Option>
              </Select>
            </Form.Item>
          </div>

          <Form.Item>
            <div style={{ display: "flex", justifyContent: "end" }}>
              <Button type="primary" htmlType="submit" style={{ borderRadius: "10px" }}>
                <span> บันทึก</span>
              </Button>
            </div>
          </Form.Item>
        </Form>
      </>
    );
  };

  return (
    <div style={{ color: "black" }}>
      <div style={{ paddingTop: "40px" }}>
        <h2>จัดการข้อมูลห้อง</h2>
        <Row style={{ paddingBottom: "20px" }}>
          <Col span={12}>
            <Button style={{ borderRadius: "10px", width: 120, height: 40 }} onClick={() => navigate(-1)}>
              <p>ย้อนกลับ</p>
            </Button>
          </Col>
          <Col span={12} style={{ display: "flex", justifyContent: "end" }}>
            <Button
              type="primary"
              style={{ borderRadius: "10px", width: 120, height: 40 }}
              onClick={() => showModalFrom("CreateRoom")}
            >
              <PlusOutlined /> <span>เพิ่มห้อง</span>
            </Button>
          </Col>
        </Row>

        <Table dataSource={dataRoom} columns={columnsRoom} bordered />
        <ModalRepair showModal={showModal} setShowModal={setShowModal}>
          <FromRoom />
        </ModalRepair>
      </div>
    </div>
  );
}
