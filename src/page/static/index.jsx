import React, { useState, useEffect } from "react";
import { getData } from "../../core/action/collection";
import { Select, DatePicker } from "antd";
import Chart from "react-apexcharts";
import styled from "styled-components";
import moment from "moment";
const { Option } = Select;
const { RangePicker } = DatePicker;

export default function Index() {
  const [data, setData] = useState([]);
  const [dataBuilding, setDataBuilding] = useState();
  const [build_id, setBuild_id] = useState();
  const [yearValue, setYearValue] = useState(moment(new Date()).format("YYYY"));
  const yearFormat = "YYYY";

  useEffect(() => {
    getDataStatic();
  }, [build_id, yearValue]);

  useEffect(() => {
    getBuilding();
    getDataStatic();
  }, []);

  const getDataStatic = async () => {
    const res = await getData(`/static?building_id=${build_id}&year_value=${yearValue}`);
    console.log("res :>> ", res);
    setData(res);
  };
  const getBuilding = async () => {
    const res = await getData("building");
    setBuild_id(res[0]?.building_id);
    setDataBuilding(res);
  };

  const Graph = () => {
    var dataGraph = {
      series: [
        {
          name: "ไม่ว่าง",
          data: data
            ? [
                data[0]?.January ? data[0]?.January : 0,
                data[0]?.February ? data[0]?.February : 0,
                data[0]?.March ? data[0]?.March : 0,
                data[0]?.April ? data[0]?.April : 0,
                data[0]?.May ? data[0]?.May : 0,
                data[0]?.June ? data[0]?.June : 0,
                data[0]?.July ? data[0]?.July : 0,
                data[0]?.August ? data[0]?.August : 0,
                data[0]?.September ? data[0]?.September : 0,
                data[0]?.October ? data[0]?.October : 0,
                data[0]?.November ? data[0]?.November : 0,
                data[0]?.December ? data[0]?.December : 0,
              ]
            : null,
        },
        {
          name: "ว่าง",
          data: data
            ? [
                data[0]?.jan && data[0]?.January > 0 ? data[0]?.jan : null,
                data[0]?.feb && data[0]?.February > 0 ? data[0]?.feb : 0,
                data[0]?.mar && data[0]?.March > 0 ? data[0]?.mar : 0,
                data[0]?.apr && data[0]?.April > 0 ? data[0]?.apr : 0,
                data[0]?.may && data[0]?.May > 0 ? data[0]?.may : 0,
                data[0]?.jun && data[0]?.June > 0 ? data[0]?.jun : 0,
                data[0]?.jul && data[0]?.July > 0 ? data[0]?.jul : 0,
                data[0]?.aug && data[0]?.August > 0 ? data[0]?.aug : 0,
                data[0]?.sep && data[0]?.September > 0 ? data[0]?.sep : 0,
                data[0]?.octo && data[0]?.October > 0 ? data[0]?.octo : 0,
                data[0]?.nov && data[0]?.November > 0 ? data[0]?.nov : 0,
                data[0]?.decem && data[0]?.December > 0 ? data[0]?.decem : 0,
              ]
            : null,
        },
      ],
      options: {
        chart: {
          type: "bar",
          height: 350,
          stacked: true,
        },
        colors: ["#E91E63", "rgb(0, 227, 150)"],
        responsive: [
          {
            breakpoint: 480,
            options: {
              legend: {
                position: "bottom",
                offsetX: -10,
                offsetY: 0,
              },
            },
          },
        ],
        yaxis: {
          min: 0,
        },
        xaxis: {
          categories: [
            "มกราคม",
            "กุมภาพันธ์",
            "มีนาคม",
            "เมษายน",
            "พฤษภาคม",
            "มิถุนายน",
            "กรกฎาคม",
            "สิงหาคม",
            "กันยายน",
            "ตุลาคม",
            "พฤศจิกายน",
            "ธันวาคม",
          ],
        },
        fill: {
          opacity: 1,
        },
        legend: {
          position: "right",
          offsetX: 0,
          offsetY: 50,
        },
      },
    };

    return <Chart options={dataGraph?.options} series={dataGraph?.series} type="bar" height={450} />;
  };

  return (
    <>
      <div style={{ padding: "20px" }}>
        <div style={{ paddingBottom: "20px" }}>
          <h3>สถิติห้องพัก</h3>
        </div>
        <div>
          <p>เลือกอาคาร</p>
          <Select
            showSearch
            style={{ width: 300 }}
            placeholder="ชื่ออาคาร"
            optionFilterProp="children"
            value={build_id}
            filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
            filterSort={(optionA, optionB) =>
              optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
            }
            onChange={(e) => setBuild_id(e)}
          >
            {dataBuilding &&
              dataBuilding.map((item) => {
                return <Option value={item.building_id}>{item.building_name}</Option>;
              })}
          </Select>
        </div>
        <div>
          <p>เลือกปี</p>
          <DatePicker
            defaultValue={moment(new Date(), "YYYY")}
            onChange={(e) => setYearValue(moment(e).format("YYYY"))}
            format={yearFormat}
            picker="year"
          />
        </div>

        <Content>
          <Graph />
        </Content>
      </div>
    </>
  );
}

const Content = styled.div`
  .apexcharts-toolbar {
    position: absolute;
    z-index: 11;
    max-width: 176px;
    text-align: right;
    border-radius: 3px;
    padding: 0px 6px 2px 6px;
    display: none;
    justify-content: space-between;
    align-items: center;
  }
`;
