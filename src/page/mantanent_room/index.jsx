/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import { Table, InputNumber, Button, Dropdown, Modal, Form, Input, Tabs, Row, Col, Menu, message } from "antd";
import {
  PlusOutlined,
  ExclamationCircleOutlined,
  BarsOutlined,
  EditOutlined,
  DeleteOutlined,
  EyeOutlined,
} from "@ant-design/icons";
import { getData, postData } from "../../core/action/collection";
import ModalRepair from "../../component/modal/";
import { CreateLog } from "../../core/util/createlog";
import { useNavigate } from "react-router-dom";
const { confirm } = Modal;

const MantanentRoom = () => {
  const [showModal, setShowModal] = useState(false);
  const [dataBuilding, setDataBuilding] = useState();
  const [typeFrom, setTypeFrom] = useState();
  const [buildingId, setBuildingId] = useState();
  const [form] = Form.useForm();
  const navigate = useNavigate();
  useEffect(() => {
    getBuilding();
  }, []);

  const menu = (data) => (
    <Menu>
      <Menu.Item>
        <p
          onClick={() =>
            setValueBuilding(
              data?.building_id,
              data?.building_name,
              data?.water_unit_price,
              data?.power_unit_price,
              data?.commonfee_price
            )
          }
        >
          <span style={{ paddingRight: "10px" }}>
            <EditOutlined />
          </span>
          แก้ไข
        </p>
      </Menu.Item>
      <Menu.Item>
        <p onClick={() => showDeleteConfirm(data.building_id, data.building_name, "Building")}>
          <span style={{ paddingRight: "10px" }}>
            <DeleteOutlined />
          </span>
          ลบ
        </p>
      </Menu.Item>
      <Menu.Item>
        <p onClick={() => navigate(`view-room/${data?.building_id}`)}>
          <span style={{ paddingRight: "10px" }}>
            <EyeOutlined />
          </span>
          ดูข้อมูล
        </p>
      </Menu.Item>
    </Menu>
  );

  const columnsBuilding = [
    {
      title: <span>#</span>,
      width: "15%",
      render: (_, data, index) => <span>{index + 1}</span>,
    },
    {
      title: <p>ชื่ออาคาร</p>,
      dataIndex: "building_name",
      key: "building_name",
      render: (_, data, index) => <span>{data?.building_name}</span>,
    },
    {
      title: <p>ค่าน้ำต่อหน่วย</p>,
      dataIndex: "water_price",
      key: "water_price",
      render: (_, data, index) => <span>{data?.water_unit_price}</span>,
    },
    {
      title: <p>ค่าไฟต่อหน่วย</p>,
      dataIndex: "power_price",
      key: "power_price",
      render: (_, data, index) => <span>{data?.power_unit_price}</span>,
    },
    {
      title: <p>ค่าส่วนกลาง</p>,
      dataIndex: "commonfee_price",
      key: "commonfee_price",
      render: (_, data, index) => <span>{data?.commonfee_price}</span>,
    },
    {
      title: <p>จัดการ</p>,
      dataIndex: "",
      key: "x",
      width: "10%",
      render: (_, data) => <Dropdown.Button icon={<BarsOutlined />} overlay={menu(data)} />,
    },
  ];

  const getBuilding = async () => {
    const res = await getData("building");
    const data = res.map((item, index) => ({
      ...item,
      key: index + 1,
    }));
    setDataBuilding(data);
  };

  const showModalFrom = (type) => {
    if (type === "CreateBuilding") {
      form.resetFields();
      setTypeFrom(type);
      setBuildingId();
      setShowModal(true);
    } else {
      setTypeFrom(type);
      setShowModal(true);
    }
  };

  const showDeleteConfirm = (id, name, type) => {
    confirm({
      title: `คุณต้องการลบ ${name} ใช่หรือไม่ ?`,
      icon: <ExclamationCircleOutlined />,
      cancelText: "ยกเลิก",
      okText: "ตกลง",
      onOk() {
        DeleteBuilding(id);
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  };

  const setValueBuilding = (id, name, water_price, power_price, commonfee_price) => {
    setBuildingId(id);
    showModalFrom("UpdateBuilding");
    form.setFieldsValue({
      buildingName: name,
      waterPrice: water_price,
      powerPrice: power_price,
      commonfeePrice: commonfee_price,
    });
  };

  const CreateBuilding = async (value) => {
    const checkName = dataBuilding?.filter((item) =>
      value.buildingName.replace(" ", "").match(item.building_name.replace(" ", ""))
    );
    if (checkName.length > 0) {
      message.error("ไม่สามารถเพิ่มชื่อตึกซ้ำกันได้");
    } else {
      const body = {
        building_name: value.buildingName,
        water_unit_price: value.waterPrice,
        power_unit_price: value.powerPrice,
        commonfee_price: value.commonfeePrice,
      };
      await postData(`/building`, { ...body });
      await getBuilding();
      await CreateLog("สร้างข้อมูลตึก");
      await setShowModal(false);
      form.resetFields();
    }
  };

  const UpdateBuilding = async (value) => {
    const checkName = dataBuilding?.filter((item) =>
      value.buildingName.replace(" ", "").match(item.building_name.replace(" ", ""))
    );
    if (checkName.length > 0) {
      message.error("ไม่สามารถเพิ่มชื่อตึกซ้ำกันได้");
    } else {
      const body = {
        building_name: value?.buildingName,
        water_unit_price: value?.waterPrice,
        power_unit_price: value?.powerPrice,
        commonfee_price: value?.commonfeePrice,
      };
      await postData(`/building/${buildingId}`, { ...body });
      await getBuilding();
      await CreateLog("อัพเดทข้อมูลตึก");
      await setShowModal(false);
      form.resetFields();
    }
  };

  const DeleteBuilding = async (value) => {
    const res = await postData(`/building/delete/${value}`);
    if (res?.data?.code !== 200) {
      message.error("ไม่สามารถลบอาคารได้เนื่องจากมีผู้พักอาศัยอยู่");
    } else {
      await CreateLog("ลบข้อมูลตึก");
      await getBuilding();
    }
  };

  const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
  };

  const FromBuilding = () => {
    return (
      <>
        <h3>รายละเอียด</h3>
        <Form
          form={form}
          onFinish={buildingId ? UpdateBuilding : CreateBuilding}
          name="insert"
          {...layout}
          initialValues={{ remember: true }}
        >
          <Form.Item
            label={<p>ชื่ออาคาร</p>}
            name="buildingName"
            rules={[{ required: true, message: "กรุณากรอก ชื่ออาคาร!" }]}
          >
            <Input style={{ width: 300 }} placeholder="ชื่ออาคาร" />
          </Form.Item>
          <Form.Item
            label={<p>ค่าน้ำต่อหน่วย</p>}
            name="waterPrice"
            rules={[{ required: true, message: "กรุณากรอก ค่าน้ำต่อหน่วย!" }]}
          >
            <InputNumber style={{ width: 300 }} min={0} placeholder="ค่าน้ำต่อหน่วย (กรอกตัวเลข)" />
          </Form.Item>
          <Form.Item
            label={<p>ค่าน้ำไฟหน่วย</p>}
            name="powerPrice"
            rules={[{ required: true, message: "กรุณากรอก ค่าน้ำไฟหน่วย!" }]}
          >
            <InputNumber style={{ width: 300 }} min={0} placeholder="ค่าน้ำไฟหน่วย (กรอกตัวเลข)" />
          </Form.Item>
          <Form.Item
            label={<p>ค่าส่วนกลาง</p>}
            name="commonfeePrice"
            rules={[{ required: true, message: "กรุณากรอก ค่าส่วนกลาง!" }]}
          >
            <InputNumber style={{ width: 300 }} min={0} placeholder="ค่าส่วนกลาง (กรอกตัวเลข)" />
          </Form.Item>
          <Form.Item>
            <div style={{ display: "flex", justifyContent: "end" }}>
              <Button type="primary" htmlType="submit" style={{ borderRadius: "10px" }}>
                <p>บันทึก</p>
              </Button>
            </div>
          </Form.Item>
        </Form>
      </>
    );
  };

  return (
    <>
      <div style={{ paddingTop: "40px" }}>
        <Row style={{ paddingBottom: "20px" }}>
          <Col span={12}>
            <h2>จัดการข้อมูลอาคาร</h2>
          </Col>
          <Col span={12} style={{ display: "flex", justifyContent: "end" }}>
            <Button
              type="primary"
              style={{ borderRadius: "10px", width: 150, height: 40 }}
              onClick={() => showModalFrom("CreateBuilding")}
            >
              <PlusOutlined /> <span>เพิ่มอาคาร</span>
            </Button>
          </Col>
        </Row>

        <Table dataSource={dataBuilding} columns={columnsBuilding} bordered />
        <ModalRepair showModal={showModal} setShowModal={setShowModal}>
          <FromBuilding />
        </ModalRepair>
      </div>
    </>
  );
};

export default MantanentRoom;
