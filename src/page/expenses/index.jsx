import React, { useEffect, useState } from "react";
import TableExpenses from "../../component/table/";
import { getData, postData } from "../../core/action/collection";
import { manageDate } from "../../component/table/manageDate";
import { ExclamationCircleOutlined } from "@ant-design/icons";
import { InputNumber, Button, Form, Select, DatePicker, Modal, message, Menu, Popconfirm, Dropdown } from "antd";
import ModalExpenses from "../../component/modal/";
import moment from "moment";
import { CreateLog } from "../../core/util/createlog";
import { BarsOutlined } from "@ant-design/icons";
import styled from "styled-components";
const { Option } = Select;
const { confirm } = Modal;

export default function Index() {
  const [data, setData] = useState();
  const [showModal, setShowModal] = useState(false);
  const [dataRoom, setDataRoom] = useState();
  const [form] = Form.useForm();
  const [bill_Id, setBill_Id] = useState();
  const [typeModel, setTypeModel] = useState();
  const [bulding, setBulding] = useState();
  const [roomId, setRoomId] = useState();
  const [dateTime, setDateTime] = useState();
  const [dataUser, setDataUser] = useState();
  const [dataBuilding, setDataBuilding] = useState();

  const menu = (data) => (
    <Menu>
      {data?.status === "0" ? (
        <div>
          {localStorage.getItem("role") === "1003" && (
            <Menu.Item>
              <p onClick={() => confirmRoomPrice(data)}>ชำระเงิน</p>
            </Menu.Item>
          )}
          <Menu.Item>
            <p onClick={() => showModalFrom("showDetail", data)}> แก้ไข</p>
          </Menu.Item>
        </div>
      ) : (
        ""
      )}
      <Menu.Item>
        <BoxPopupConfirm
          className="popup-booking"
          title="ต้องการลบข้อมูลหรือไม่ ?"
          okText="ตกลง"
          onConfirm={() => confirmDelete(data)}
          cancelText="ยกเลิก"
        >
          <p>ลบ</p>
        </BoxPopupConfirm>
      </Menu.Item>
    </Menu>
  );

  const columnsExpenses = [
    {
      title: "#",
      width: 50,
      render: (_, data, index) => <span>{index + 1}</span>,
    },
    {
      title: "ข้อมูลผู้พักอาศัย",
      dataIndex: "sum_water",
      width: 150,
      render: (_, data, index) => (
        <div>
          <p>
            {data.fname} {data.lname}
          </p>
          <p>
            {data.building_name} / ห้อง {data.room_name}
          </p>
        </div>
      ),
    },

    {
      title: "หน่วยค่าน้ำ",
      dataIndex: "water",
      width: 150,
      render: (_, data) => (
        <div>
          <p>เดือนก่อน: {data.prev_water_unit} หน่วย</p>
          <p>เดือนปัจจุบัน: {data.current_water_unit} หน่วย</p>
          {/* <p>ราคา: {data.current_water_unit === "0" ? "0" + " บาท" : data.water_unit_price + " บาท"} </p> */}
        </div>
      ),
    },
    {
      title: "หน่วยค่าไฟ",
      dataIndex: "power",
      width: 150,
      render: (_, data) => (
        <div>
          <p>เดือนก่อน: {data.prev_power_unit} หน่วย</p>
          <p>เดือนปัจจุบัน: {data.current_power_unit} หน่วย</p>
          {/* <p>ราคา: {data.current_power_unit === "0" ? "0" + " บาท" : data.power_unit_price + " บาท"}</p> */}
        </div>
      ),
    },
    {
      title: "วันที่",
      dataIndex: "bill_month",
      width: 110,
      render: (_, data) => <span>{manageDate(data?.bill_month)}</span>,
    },
    {
      title: "การชำระเงิน",
      dataIndex: "status",
      width: 100,
      render: (_, data) => (
        <span>
          {data.status === "0" ? (
            <p style={{ color: "red" }}>ยังไม่ชำระ</p>
          ) : (
            <div>
              <p style={{ color: "green" }}>ชำระเเล้ว</p>
              <p>{manageDate(data.date_pay)}</p>
            </div>
          )}
        </span>
      ),
    },
    {
      title: "จัดการชำระเงิน",
      width: 70,
      render: (_, data) => <Dropdown.Button icon={<BarsOutlined />} overlay={menu(data)} />,
    },
  ];

  useEffect(() => {
    getRoomData();
    getUser();
    getBuilding();
    getDataExpenses();
  }, []);

  const getUser = async () => {
    const res = await getData(`/expenses/UserExpenses/${localStorage.getItem("id")}`);
    await setDataUser(res);
  };

  const getBuilding = async () => {
    const res = await getData("building");
    setDataBuilding(res);
  };
  const showModalFrom = (type, data) => {
    if (type === "save_water") {
      form.resetFields();
      setTypeModel(type);
      setShowModal(true);
      form.setFieldsValue({
        Building: dataUser?.building_id,
        room_id: dataUser?.room_id,
      });
    } else if (type === "UpdateWaterUnit") {
      setBill_Id(data.bill_id);
      setShowModal(true);
      setTypeModel(type);
      form.setFieldsValue({
        water_unit: data?.current_water,
      });
    } else if (type === "showDetail") {
      setBill_Id(data.bill_id);
      setShowModal(true);
      setTypeModel(type);

      form.setFieldsValue({
        Building: data?.building_id,
        room_id: data?.room_name,
        waterUnit: data?.current_water_unit,
        powerUnit: data?.current_power_unit,
        dateAt: moment(data?.bill_month),
      });
    } else if (type === "createBill") {
      setShowModal(true);
      setTypeModel(type);
      form.resetFields();
    } else {
      setTypeModel(type);
      setShowModal(true);
    }
  };

  const UpdateRoomPrice = async (data) => {
    // console.log("data", data);
    await postData(`/bill/updateBill/${data?.bill_id}`);
    await getDataExpenses(dateTime);
  };

  const confirmRoomPrice = async (data) => {
    confirm({
      title: `ยืนยันการชำระเงิน`,
      icon: <ExclamationCircleOutlined />,
      cancelText: "ยกเลิก",
      okText: "ตกลง",
      onOk() {
        UpdateRoomPrice(data);
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  };

  const confirmDelete = async (data) => {
    confirm({
      title: `ยืนยันการลบ`,
      icon: <ExclamationCircleOutlined />,
      cancelText: "ยกเลิก",
      okText: "ตกลง",
      onOk() {
        deleteBill(data?.bill_id);
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  };
  const getRoomData = async (value) => {
    const res = await getData("/room");
    const data = res.filter((item, index, array) => item.building_id === value);
    form.setFieldsValue({
      room_id: data[0]?.room_id,
    });
    setDataRoom(value ? data : res);
  };

  const deleteBill = async (id) => {
    const res = await postData(`/bill/delete/${id}`);
    if (res?.data?.code === 200) {
      await getDataExpenses();
      await CreateLog("ลบข้อมูลบิล");
      await message.success("ลบข้อมูลสำเร็จ", 3);
    }
  };

  const getDataExpenses = async (dateString, type) => {
    if (localStorage.getItem("role") === "1003") {
      const response = await getData(`/bill`);
      if (!response) {
        await setData();
      } else {
        const dataExpenses = response
          .sort(function compareFn(a, b) {
            return moment(a.bill_month) - moment(b.bill_month);
          })
          .map((item, index) => ({
            ...item,
            key: index + 1,
          }));
        const dataFilter = dataExpenses?.filter(
          (item) =>
            moment(item.bill_month).format("YYYY-MM") === moment(dateString).format("YYYY-MM") &&
            item.room_id === roomId &&
            item?.building_id === bulding
        );
        await setData(type === "search" ? dataFilter : dataExpenses);
      }
    } else {
      const response = await getData(`/bill`);
      const dataExpenses = response?.filter((item) => item?.user_id === localStorage.getItem("id"));
      const dataFilter = dataExpenses?.filter(
        (item) => moment(item.bill_month).format("YYYY-MM") === moment(dateString).format("YYYY-MM")
      );

      if (response) {
        setData(type === "search" && dateString ? dataFilter : dataExpenses);
      } else {
        setData();
      }
    }
  };

  const CreateBil = async (value) => {
    const body = {
      room_id: value?.room_id,
      water_unit: value?.waterUnit,
      power_unit: value?.powerUnit,
      date_at: moment(value?.dateAt).format("YYYY-MM"),
    };
    const response = await postData("/bill", { ...body });
    if (response?.data === "got a bill this month") {
      message.error("ไม่สามารถสร้างบิลย้อนหลังได้ หรือ เดือนนี้อาจจะมีการสร้างบิลมาแล้ว", 5);
    } else if (response?.data === "water unit incorrect") {
      message.error("ไม่สามารถใส่หน่วยค่าน้ำย้อนหลังได้", 5);
    } else if (response?.data === "power_unit incorrect") {
      message.error("ไม่สามารถใส่หน่วยค่าไฟย้อนหลังได้", 5);
    } else if (response?.data === "room id not found") {
      message.error("หมายเลขห้องยังไม่ถูกอนุญาตให้เข้าพัก", 5);
    } else {
      await getDataExpenses();
      setShowModal(false);
      await CreateLog("สร้างข้อมูลบิล");
      message.success("เพิ่มบิลสำเร็จ");
    }
  };

  const UpdateBill = async (value) => {
    const body = {
      id: bill_Id,
      water_unit: value?.waterUnit,
      power_unit: value?.powerUnit,
      date_ex: moment(value?.dateAt).format("YYYY-MM"),
    };
    const res = await postData(`/bill/${bill_Id}`, { ...body });
    if (res?.data === "not the latest bill") {
      message.error("ไม่สามารถแก้ไขบิลย้อนหลังได้");
    } else {
      await getDataExpenses(dateTime);
      await setShowModal(false);
      await CreateLog("แก้ไขข้อมูลบิล");
      message.success("แก้ไขบิลสำเร็จ");
    }
  };

  const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
  };

  async function onChange(date, dateString) {
    setDateTime(dateString);
  }

  const onChangeBuilding = (value) => {
    setBulding(value);
    getRoomData(value);
  };
  const onChangeSearchUser = (value) => {
    getDataExpenses(value, "search");
  };

  const getBill = async () => {
    await getDataExpenses(dateTime, "search");
  };

  const FromExpemses = () => {
    return (
      <>
        <h4>รายละเอียด</h4>
        <Form
          name="insert"
          {...layout}
          onFinish={typeModel === "showDetail" ? UpdateBill : CreateBil}
          form={form}
          initialValues={{ remember: true }}
        >
          <Form.Item label="ชื่ออาคาร" name="Building" rules={[{ required: true, message: "กรุณากรอก ชื่ออาคาร!" }]}>
            <Select
              showSearch
              style={{ width: 300 }}
              placeholder="ชื่ออาคาร"
              optionFilterProp="children"
              onChange={onChangeBuilding}
              disabled={localStorage.getItem("role") !== "1003" ? true : false}
              filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
            >
              {dataBuilding &&
                dataBuilding.map((item, index) => {
                  return (
                    <Option key={`Expemses${index}`} value={item.building_id}>
                      {item.building_name}
                    </Option>
                  );
                })}
            </Select>
          </Form.Item>
          <Form.Item label="ชื่อห้อง" name="room_id" rules={[{ required: true, message: "กรุณากรอก ชื่อห้อง!" }]}>
            <Select
              showSearch
              style={{ width: 300 }}
              placeholder="ชื่อห้อง"
              optionFilterProp="children"
              disabled={localStorage.getItem("role") !== "1003" ? true : false}
              filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
              filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
              }
            >
              {dataRoom &&
                dataRoom.map((item, index) => {
                  return (
                    <Option key={`Expemses${index}`} value={item.room_id}>
                      {item.room_name}
                    </Option>
                  );
                })}
            </Select>
          </Form.Item>

          <Form.Item
            label="หน่วยค่าน้ำปัจจุบัน"
            name="waterUnit"
            rules={[{ required: true, message: "กรุณากรอก หน่วยค่าน้ำ!" }]}
          >
            <InputNumber style={{ width: 300 }} min={0} placeholder="หน่วยค่าน้ำ (กรอกตัวเลข)" />
          </Form.Item>
          <Form.Item
            label="หน่วยค่าไฟปัจจุบัน"
            name="powerUnit"
            rules={[{ required: true, message: "กรุณากรอก หน่วยค่าไฟ!" }]}
          >
            <InputNumber style={{ width: 300 }} min={0} placeholder="หน่วยค่าไฟ (กรอกตัวเลข)" />
          </Form.Item>
          {typeModel !== "showDetail" && (
            <Form.Item label="วันที่" name="dateAt" rules={[{ required: true, message: "กรุณากรอก วันที่!" }]}>
              <DatePicker
                picker="month"
                placeholder="เลือกวันที่"
                disabledDate={(current) => {
                  return current && current >= moment(new Date()).add(0, "month");
                }}
              />
            </Form.Item>
          )}

          <Form.Item>
            <div style={{ display: "flex", justifyContent: "end" }}>
              <Button htmlType="submit">บันทึก</Button>
            </div>
          </Form.Item>
        </Form>
      </>
    );
  };

  return (
    <div>
      <h2>สรุปค่าใช้จ่าย</h2>
      {localStorage.getItem("role") !== "1003" ? (
        <div>
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <div>
              <span style={{ paddingRight: "20px", fontSize: "18px" }}>
                ชื่อผู้พักอาศัย: {dataUser && dataUser?.fname} {dataUser && dataUser?.lname}
              </span>
              <span style={{ fontSize: "18px" }}>
                หมายเลขห้อง: {dataUser && dataUser?.room_name} {dataUser && dataUser?.building_name}
              </span>
              <div style={{ padding: "10px 0px 10px 0px" }}>
                <span style={{ fontSize: "18px" }}>เลือกวันที่ : </span>
                <span style={{ padding: "0px 0px 0px 10px" }}>
                  <DatePicker onChange={onChangeSearchUser} picker="month" placeholder="เลือกวันที่" />
                </span>
              </div>
            </div>
            <Button type="primary" onClick={() => showModalFrom("save_water")}>
              บันทึกค่าน้ำ-ไฟ
            </Button>
          </div>
          <TableExpenses columns={columnsExpenses} dataSource={data} />
          <ModalExpenses showModal={showModal} setShowModal={setShowModal}>
            <FromExpemses />
          </ModalExpenses>
        </div>
      ) : (
        <div>
          <div
            style={{
              paddingBottom: "20px",
              display: "flex",
              justifyContent: "space-between",
            }}
          >
            <div style={{ display: "flex", gap: "10px" }}>
              <div>
                <p>เลือกวันที่</p>
                <DatePicker
                  disabledDate={(current) => {
                    return current && current >= moment(new Date()).add(0, "month");
                  }}
                  picker="month"
                  onChange={(e) => setDateTime(e._d)}
                  placeholder="เลือกวันที่"
                />
              </div>
              {localStorage.getItem("role") === "1003" && (
                <div style={{ display: "flex", gap: "10px" }}>
                  <div>
                    <p>เลือกอาคาร</p>
                    <Select
                      showSearch
                      style={{ width: 300 }}
                      placeholder="ชื่ออาคาร"
                      optionFilterProp="children"
                      onChange={onChangeBuilding}
                      filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                      filterSort={(optionA, optionB) =>
                        optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
                      }
                    >
                      {dataBuilding &&
                        dataBuilding.map((item) => {
                          return <Option value={item.building_id}>{item.building_name}</Option>;
                        })}
                    </Select>
                  </div>
                  <div>
                    <p>เลือกห้อง</p>
                    <Select
                      showSearch
                      style={{ width: 300 }}
                      placeholder="ชื่อห้อง"
                      optionFilterProp="children"
                      onChange={(e) => setRoomId(e)}
                      filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                      filterSort={(optionA, optionB) =>
                        optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
                      }
                    >
                      {dataRoom &&
                        dataRoom?.map((item, index) => {
                          return (
                            <Option key={`Expemses${index}`} value={item.room_id}>
                              {item.room_name}
                            </Option>
                          );
                        })}
                    </Select>
                  </div>
                </div>
              )}

              <BoxBtnSearch>
                <Button className="btn-search-bill" onClick={getBill}>
                  ค้นหา
                </Button>
              </BoxBtnSearch>
            </div>
            <BoxCheckBill>
              <Button className="btn-check-bill" onClick={() => showModalFrom("createBill")}>
                บันทึกค่าน้ำ-ไฟ
              </Button>
            </BoxCheckBill>
          </div>
          <TableExpenses columns={columnsExpenses} dataSource={data} />
          <ModalExpenses showModal={showModal} setShowModal={setShowModal}>
            <FromExpemses />
          </ModalExpenses>
        </div>
      )}
    </div>
  );
}

const BoxBtnSearch = styled.div`
  padding-top: 28px;
  .btn-search-bill {
    height: 35px;
  }
`;

const BoxCheckBill = styled.div`
  display: flex;
  align-items: center;
  .btn-check-bill {
    height: 35px;
  }
`;

const BoxPopupConfirm = styled(Popconfirm)`
  .popup-booking {
    .ant-popover-buttons button {
      margin-left: 8px;
      height: 35px !important;
    }
  }
`;
