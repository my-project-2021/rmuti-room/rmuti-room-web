import "./App.css";
import { Routes, Route, Navigate, Outlet } from "react-router-dom";
import React, { useEffect } from "react";
import "antd/dist/antd.css";
import Login from "./page/login";
import Register from "./page/register";
import Relations from "./page/relations";
import Waiting from "./page/waiting/";
import MantanentRoom from "./page/mantanent_room";
import ViewRoom from "./page/viewRoom";
import ViewUser from "./page/viewUser";
import Expenses from "./page/expenses/";
import Repair from "./page/repair/";
import Static from "./page/static/";
import MantanentUser from "./page/mantanent_user";
import PeviewNew from "./page/relations/peview";
import About from "./page/about/";
import News from "./page/news/";
import PriceCenter from "./page/priceCenter";
import Abouts from "./page/about";
import { createBrowserHistory } from "history";
import CoreLayout from "./component/layout";
const history = createBrowserHistory();

const App = () => {
  // useEffect(() => {
  //   console.log("popstate");
  //   window.onpopstate = function (event) {
  //     console.log("first", event);
  //     console.log(event);
  //     if (event.state === null) {
  //       if (window.confirm("are you sure to go back!") === true) window.history.back();
  //     } else {
  //       window.confirm("new page!");
  //     }
  //   };
  // }, []);

  return (
    <>
      <Routes history={history}>
        {localStorage.getItem("id") ? (
          <>
            {localStorage.getItem("status_id") === "2" && (
              <Route path="/" element={<CoreLayout />}>
                <Route path="*" element={<Navigate to="/about" />} />
                <Route path="/about" element={<Abouts />} />
                <Route path="/mantanent_room" element={<Outlet />}>
                  <Route path="view-room/:id" element={<Outlet />}>
                    <Route path="view-user/:id" element={<ViewUser />} />
                    <Route index element={<ViewRoom />} />
                  </Route>
                  <Route index element={<MantanentRoom />} />
                </Route>
                <Route path="/news" element={<News />} />
                <Route path="/mantanent" element={<MantanentUser />} />
                <Route path="/save_center_price" element={<PriceCenter />} />
                <Route path="/repair" element={<Repair />} />
                <Route path="/static_room" element={<Static />} />
                <Route path="/expenses" element={<Expenses />} />
                <Route path="/report" element={<MantanentRoom />} />
                <Route path="/about" element={<About />} />
              </Route>
            )}
          </>
        ) : (
          <>
            <Route path="/login" element={<Login />} />
            <Route path="/peview-news/:id" element={<PeviewNew />} />
            <Route path={`/waiting`} element={<Waiting />} />
            <Route path={`/role/:id`} element={<Waiting />} />
            <Route path={`/register/:id`} element={<Register />} />
            <Route path="*" element={<Relations />} />
          </>
        )}
      </Routes>
    </>
  );
};

export default App;
