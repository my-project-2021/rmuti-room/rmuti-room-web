import axios from "axios";

const httpClient = axios.create({
  baseURL: "https://dev.cpe.rmuti.ac.th/pws/rmuti-room-service/public/",
  // baseURL: "http://localhost/rmuti-room-service/public",
  headers: {
    "Content-Type": "application/json",
  },
});

export default httpClient;
