import httpClient from "./httpClient";

export const getData = async (path, params) => {
  // 3 เป็น function สำหรับ import ไปใช้หน้าอื่น ที่ต้องการ เรียก api โดยใช้ method get
  try {
    const response = await httpClient.get(path, { ...params });
    return response.data.data;
  } catch (err) {
    return [];
  }
  // 5-10 จะใช้ try เพื่อเช็คการ error ของ response หาก response มีข้อมูลจะ return ข้อมูลจาก api ออกไป ถ้า error จะ return array ว่าง
};

export const postData = async (path, body) => {
  if (!body) {
    try {
      const response = await httpClient.post(path);
      return response;
    } catch (err) {
      return err?.response?.data;
    }
  } else {
    try {
      const response = await httpClient.post(path, body);
      return response;
    } catch (err) {
      return err?.response?.data;
    }
  }
};

export const putData = async (path, body) => {
  try {
    const response = await httpClient.put(path, { ...body });
    return response;
  } catch (err) {
    return [];
  }
};

export const deleteData = async (path, params) => {
  try {
    const response = await httpClient.delete(path);
    return response;
  } catch (err) {
    return [];
  }
};
