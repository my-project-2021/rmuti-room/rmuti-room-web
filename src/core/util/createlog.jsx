import { getData, postData } from "../action/collection";

export const CreateLog = async (detail) => {
  const body = {
    user_id: localStorage.getItem("id"),
    room_id: localStorage.getItem("room_id") !== "undefined" ? localStorage.getItem("room_id") : null,
    detail: detail,
  };
  const res = await postData(`/log`, { ...body });
};
