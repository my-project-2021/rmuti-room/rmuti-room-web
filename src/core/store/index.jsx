import React, { useContext, useState } from 'react'

const AuthContext = React.createContext()

export function useFeature() {
  return useContext(AuthContext)
}

export function AuthProvider({ children }) {
  const [isLogin, setIsLogin] = useState(false)

  const value = {
    isLogin,
    setIsLogin,
  }

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>
}
