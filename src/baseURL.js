export const BaseURL = {
  loginSSO: "https://www.cpe.rmuti.ac.th/sso/login/95861644-d812-4d91-8c17-a769c232f471",
  logoutSSO: "https://www.cpe.rmuti.ac.th/sso/logout/95861644-d812-4d91-8c17-a769c232f471",
  ImageURL: "https://dev.cpe.rmuti.ac.th/pws/rmuti-room-service/src/images/",
  // ImageURL: "http://localhost/rmuti-room-service/src/images/",
};
