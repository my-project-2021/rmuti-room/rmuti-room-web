import React from "react";
import { Table } from "antd";

export default function index({ columns, dataSource }) {
  return (
    <div>
      <Table columns={columns} dataSource={dataSource} bordered />
    </div>
  );
}
