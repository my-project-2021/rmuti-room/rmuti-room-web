let monthNames = [
  "มกราคม",
  "กุมภาพันธ์",
  "มีนาคม",
  "เมษายน",
  "พฤษภาคม",
  "มิถุนายน",
  "กรกฎาคม",
  "สิงหาคม",
  "กันยายน",
  "ตุลาคม",
  "พฤศจิกายน",
  "ธันวาคม",
];

export const manageDate = (data) => {
  if (data) {
    return `${
      new Date(data).getDate() +
      " " +
      monthNames[new Date(data).getMonth()] +
      " " +
      (new Date(data).getFullYear() + 543)
    }`;
  }
};
