import React from "react";

export default function Index({ children }) {
  return (
    <div className="content" style={{ height: "100%" }}>
      {children}
    </div>
  );
}
