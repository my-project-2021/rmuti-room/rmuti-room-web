import React from "react";
import Content from "./content";
import Header from "./header";
import Sider from "./sider";
import { Outlet } from "react-router-dom";

export default function Index() {
  return (
    <div>
      <div style={{ display: "flex", backgroundColor: "#f6f6f6" }}>
        <Sider />
        <div
          style={{
            width: "100%",
            height: "100vh",
            display: "grid",
            gridTemplateRows: "50px auto",
          }}
        >
          <Header />
          <div style={{ padding: "35px", height: "100%", overflow: "auto" }}>
            <Content>
              <Outlet />
            </Content>
          </div>
        </div>
      </div>
    </div>
  );
}
