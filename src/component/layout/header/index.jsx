import React, { useState, useEffect } from "react";
import { Row, Col, Button } from "antd";
import styled from "styled-components";
import { BaseURL } from "../../../baseURL";
import { getData } from "../../../core/action/collection";
import { LogoutOutlined } from "@ant-design/icons";
import { CreateLog } from "../../../core/util/createlog";
export default function Index() {
  const [dataUser, setDataUser] = useState();
  const onLogout = async () => {
    await CreateLog("logout");
    await localStorage.clear();
    await window.location.replace(BaseURL.logoutSSO);
  };
  useEffect(() => {
    getListUser();
  }, []);
  const getListUser = async () => {
    const res = await getData("/user");
    const result = res.filter((item) => item.user_id === localStorage.getItem("id"));
    const data = result.map((item, index) => ({
      ...item,
      key: index + 1,
    }));
    await setDataUser(data);
  };
  return (
    <Box>
      <Row justify="space-between">
        <Col span={24} style={{ padding: "20px 40px", display: "flex", justifyContent: "end" }}>
          <Col style={{ color: "black" }}>
            <span>ชื่อผู้ใช้งาน:</span> <TextHeader>{dataUser && dataUser[0]?.fname}</TextHeader>
            <TextHeader>{dataUser && dataUser[0]?.lname}</TextHeader>
            <p> สถานะ: {dataUser && dataUser[0]?.type_name}</p>
          </Col>
          <Col>
            <Button
              style={{ borderRadius: "10px", width: 120, height: 40 }}
              type="primary"
              icon={<LogoutOutlined />}
              onClick={onLogout}
              ghost
            >
              <span>Logout</span>
            </Button>
          </Col>
        </Col>
      </Row>
    </Box>
  );
}

const Box = styled.div`
  background-color: white;
  height: 80px;
  z-index: 999;
`;

const TextHeader = styled.span`
  padding-right: 10px;
`;
