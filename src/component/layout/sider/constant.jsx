import {
  HomeOutlined,
  AuditOutlined,
  SolutionOutlined,
  FileDoneOutlined,
  ControlOutlined,
  BarChartOutlined,
  ReadOutlined,
  ContainerOutlined,
} from "@ant-design/icons";

export const MenuAdmin = [
  {
    key: 1,
    slug: "About",
    value: "หน้าหลัก",
    icon: <HomeOutlined />,
  },
  {
    key: 2,
    slug: "mantanent_room",
    value: "จัดการห้องพักอาศัย",
    icon: <AuditOutlined />,
  },
  {
    key: 3,
    slug: "expenses",
    value: "สรุปอัตราการใช้น้ำ-ไฟ",
    icon: <FileDoneOutlined />,
  },
  {
    key: 4,
    slug: "mantanent",
    value: "จัดการข้อมูลผู้พักอาศัย",
    icon: <SolutionOutlined />,
  },
  { key: 5, slug: "repair", value: "แจ้งซ่อม", icon: <ControlOutlined /> },
  { key: 6, slug: "static_room", value: "แสดงสถิติห้องพัก", icon: <BarChartOutlined /> },
  { key: 7, slug: "news", value: "จัดการข้อมูลข่าวสาร", icon: <ReadOutlined /> },
];

export const MenuSell = [
  {
    key: 1,
    slug: "About",
    value: "หน้าหลัก",
    icon: <HomeOutlined />,
  },
  {
    key: 2,
    slug: "save_center_price",
    value: "จัดการค่าส่วนกลาง",
    icon: <ContainerOutlined />,
  },
  {
    key: 3,
    slug: "expenses",
    value: "สรุปอัตราการใช้น้ำ-ไฟ",
    icon: <FileDoneOutlined />,
  },
  { key: 4, slug: "repair", value: "แจ้งซ่อม", icon: <ControlOutlined /> },
];

export const MenuUser = [
  {
    key: 1,
    slug: "About",
    value: "หน้าหลัก",
    icon: <HomeOutlined />,
  },
  {
    key: 2,
    slug: "expenses",
    value: "สรุปอัตราการใช้น้ำ-ไฟ",
    icon: <FileDoneOutlined />,
  },
  {
    key: 3,
    slug: "save_center_price",
    value: "ค่าส่วนกลาง",
    icon: <ContainerOutlined />,
  },
  { key: 4, slug: "repair", value: "แจ้งซ่อม", icon: <ControlOutlined /> },
];
