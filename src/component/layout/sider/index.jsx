import React, { useState } from "react";
import { Menu, Button, Tooltip } from "antd";

import classNames from "classnames";

import styled from "styled-components";
import { useNavigate } from "react-router-dom";

import { MenuAdmin, MenuSell, MenuUser } from "./constant";

export default function Index() {
  const [collapAction, setCollapAction] = useState(false);
  const [slugSelect, setSlugSelect] = useState("");

  const navigate = useNavigate();
  const onClickMenu = (value) => {
    navigate(`/${value?.slug}`);
    setSlugSelect(value?.slug);
  };
  const onCollapse = () => {
    setCollapAction(!collapAction);
  };

  return (
    <>
      <Content collapAction={collapAction}>
        <div className="sider">
          <div id="main" className="container"></div>
          <div className="sider-menu">
            <img
              src={"https://rmuti.ac.th/one/wp-content/uploads/2021/12/RMUTI_KORAT.png"}
              className="responsive"
              width="70"
              height="70"
              alt="Logo"
            />
            {localStorage.getItem("role") === "1003" ? (
              MenuAdmin.map((item, index) => {
                return (
                  <Tooltip key={`sider${index}`} placement="rightTop" title={item?.value}>
                    <div
                      className={classNames({
                        "text-menu": true,
                        selected: item?.slug === slugSelect,
                      })}
                      tabIndex={1}
                      onClick={() => onClickMenu(item)}
                    >
                      <TextMenu collapAction={collapAction}>
                        <span className="icon"> {item?.icon}</span>
                        {item?.value}
                      </TextMenu>
                    </div>
                  </Tooltip>
                );
              })
            ) : localStorage.getItem("role") === "1002" ? (
              MenuSell.map((item, index) => {
                return (
                  <Tooltip key={`sider${index}`} placement="rightTop" title={item?.value}>
                    <div
                      className={classNames({
                        "text-menu": true,
                        selected: item?.slug === slugSelect,
                      })}
                      tabIndex={1}
                      onClick={() => onClickMenu(item)}
                    >
                      <TextMenu collapAction={collapAction}>
                        <span className="icon"> {item?.icon}</span>
                        {item?.value}
                      </TextMenu>
                    </div>
                  </Tooltip>
                );
              })
            ) : localStorage.getItem("role") === "1001" ? (
              MenuUser?.map((item, index) => {
                return (
                  <Tooltip key={`sider${index}`} placement="rightTop" title={item?.value}>
                    <div
                      className={classNames({
                        "text-menu": true,
                        selected: item?.slug === slugSelect,
                      })}
                      tabIndex={1}
                      onClick={() => onClickMenu(item)}
                    >
                      <TextMenu collapAction={collapAction}>
                        <span className="icon"> {item?.icon}</span>
                        {item?.value}
                      </TextMenu>
                    </div>
                  </Tooltip>
                );
              })
            ) : (
              <Menu theme="dark" mode="inline"></Menu>
            )}
            <BtCollap
              className="arrow"
              type="primary"
              shape="circle"
              //   icon={<ArrowRightOutlined />}
              onClick={() => onCollapse()}
            />
          </div>
        </div>
      </Content>
    </>
  );
}

const Content = styled.div`
  .responsive {
    height: auto;
  }
  .sider-menu {
    position: relative;
    padding: 10px;
    padding-top: 20px;
    height: 100vh;
    transition: 0.2s;
    width: ${(props) => (props.collapAction ? "70px" : "240px")};
    background-image: linear-gradient(30deg, #0048bd, #44a7fd);
    color: black;
    text-align: center;
  }
  .icon {
    padding-right: 10px;
  }
  .sub-sider-menu {
    transition: 0.2s;
    transition: 0.5s;
  }

  .text-menu {
    transition: color 0.2s;
    vertical-align: middle;
    white-space: nowrap;
    border-radius: 10px;
    height: 40px;
    margin: 10px 0 0 0;
    :hover,
    :focus,
    &.selected {
      cursor: pointer;
      background-color: rgba(255, 255, 255, 0.21);
      color: #fff;
    }
    justify-content: ${(props) => (props.collapAction ? "center" : "")};
  }
  .arrow {
    right: -16px;
    position: absolute;
    bottom: 45px;
    transition: 0.2s;
    transform: ${(props) => (props.collapAction ? "rotate(0deg)" : "rotate(-180deg)")};
  }
`;
const BtCollap = styled(Button)`
  border: none;
  background: white;
  color: #00b7ad;
  :focus,
  :hover {
    background-color: white;
    color: #00b7ad;
  }
`;
const TextMenu = styled.span`
  display: ${(props) => (props.collapAction ? "none" : "block")};
  text-align: left;
  align-self: center;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
  word-wrap: break-word;
  padding: 10px;
`;
