import React from "react";
import { Modal } from "antd";
export default function Index({ textTitle, setShowModal, disableModal, showModal, children, width }) {
  return (
    <Modal
      title={<p>{textTitle}</p>}
      visible={showModal}
      onOk={disableModal}
      onCancel={() => setShowModal(false)}
      footer={false}
      width={width}
    >
      {children}
    </Modal>
  );
}
